/*
Navicat MySQL Data Transfer

Source Server         : Personal projects
Source Server Version : 50505
Source Host           : localhost:3306
Source Database       : tosalamour

Target Server Type    : MYSQL
Target Server Version : 50505
File Encoding         : 65001

Date: 2017-05-09 04:54:34
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for tsl_categories
-- ----------------------------
DROP TABLE IF EXISTS `tsl_categories`;
CREATE TABLE `tsl_categories` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `cate_code` varchar(10) DEFAULT NULL,
  `cate_name` varchar(50) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `is_delete` int(1) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of tsl_categories
-- ----------------------------
INSERT INTO `tsl_categories` VALUES ('1', 'A', 'Áo cưới', '2016-10-02 17:48:41', '2017-02-23 03:44:10', '0');
INSERT INTO `tsl_categories` VALUES ('2', 'P', 'Phụ kiện', '2016-10-05 22:20:06', '2017-02-22 10:18:30', '0');
INSERT INTO `tsl_categories` VALUES ('3', 'D', 'Áo dài nữ', '2016-10-05 22:21:37', '2017-02-22 10:19:04', '0');
INSERT INTO `tsl_categories` VALUES ('4', 'DN', 'Áo dài nam', '2016-10-05 22:22:22', '2016-10-06 00:20:46', '0');
INSERT INTO `tsl_categories` VALUES ('5', 'Q', 'Áo dài bưng quả nữ', '2016-10-06 00:23:39', '2016-10-06 00:25:35', '0');
INSERT INTO `tsl_categories` VALUES ('6', 'QN', 'Áo dài bưng quả nam', '2016-10-06 00:23:42', '2016-10-06 00:25:27', '0');
INSERT INTO `tsl_categories` VALUES ('7', 'V', 'Vest', '2016-10-06 00:24:05', '2016-10-08 19:29:06', '0');

-- ----------------------------
-- Table structure for tsl_customers
-- ----------------------------
DROP TABLE IF EXISTS `tsl_customers`;
CREATE TABLE `tsl_customers` (
  `id` int(5) NOT NULL,
  `cus_name` varchar(255) DEFAULT NULL,
  `cus_address` varchar(255) DEFAULT NULL,
  `cus_email` varchar(255) DEFAULT NULL,
  `cus_note` text,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `is_delete` int(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of tsl_customers
-- ----------------------------

-- ----------------------------
-- Table structure for tsl_orderdetails
-- ----------------------------
DROP TABLE IF EXISTS `tsl_orderdetails`;
CREATE TABLE `tsl_orderdetails` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `order_id` int(5) DEFAULT NULL,
  `pro_id` int(5) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `is_delete` int(1) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of tsl_orderdetails
-- ----------------------------

-- ----------------------------
-- Table structure for tsl_orders
-- ----------------------------
DROP TABLE IF EXISTS `tsl_orders`;
CREATE TABLE `tsl_orders` (
  `id` int(5) NOT NULL,
  `year_order` varchar(2) DEFAULT NULL,
  `order_code` varchar(10) DEFAULT NULL,
  `cus_id` int(5) DEFAULT NULL,
  `rent_date` datetime DEFAULT NULL,
  `back_date` datetime DEFAULT NULL,
  `deposit` double DEFAULT NULL,
  `caution_money` double DEFAULT NULL,
  `total_rent_money` double DEFAULT NULL,
  `broken_refund` double DEFAULT NULL,
  `total_refund` double DEFAULT NULL,
  `is_finish` int(1) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of tsl_orders
-- ----------------------------

-- ----------------------------
-- Table structure for tsl_photos
-- ----------------------------
DROP TABLE IF EXISTS `tsl_photos`;
CREATE TABLE `tsl_photos` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `img_name` varchar(255) DEFAULT NULL,
  `pro_id` int(5) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `is_delete` int(1) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=37 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of tsl_photos
-- ----------------------------
INSERT INTO `tsl_photos` VALUES ('1', 'A0002.jpg', '2', '2017-01-18 23:16:22', '2017-03-07 10:51:54', '0');
INSERT INTO `tsl_photos` VALUES ('2', 'A0004.jpg', '4', '2017-01-18 23:16:22', '2017-01-18 23:16:22', '0');
INSERT INTO `tsl_photos` VALUES ('3', 'A0003.jpg', '3', '2017-01-18 23:16:22', '2017-01-18 23:16:22', '0');
INSERT INTO `tsl_photos` VALUES ('4', 'A0004_back.jpg', '4', '2017-01-18 23:16:23', '2017-01-18 23:16:23', '0');
INSERT INTO `tsl_photos` VALUES ('5', 'A0001.jpg', '1', '2017-01-18 23:16:23', '2017-01-18 23:16:23', '0');
INSERT INTO `tsl_photos` VALUES ('6', 'A0004_front.jpg', '4', '2017-01-18 23:16:24', '2017-01-18 23:16:24', '0');
INSERT INTO `tsl_photos` VALUES ('7', 'A0005.jpg', '5', '2017-01-18 23:16:24', '2017-01-18 23:16:24', '0');
INSERT INTO `tsl_photos` VALUES ('8', 'A0005_back.jpg', '5', '2017-01-18 23:16:24', '2017-01-18 23:16:24', '0');
INSERT INTO `tsl_photos` VALUES ('9', 'A0005_side.jpg', '5', '2017-01-18 23:18:16', '2017-01-18 23:18:16', '0');
INSERT INTO `tsl_photos` VALUES ('10', 'A0007.jpg', '7', '2017-01-18 23:18:17', '2017-01-18 23:18:17', '0');
INSERT INTO `tsl_photos` VALUES ('11', 'A0005_front_1.jpg', '5', '2017-01-18 23:18:17', '2017-01-18 23:18:17', '0');
INSERT INTO `tsl_photos` VALUES ('12', 'A0006.jpg', '6', '2017-01-18 23:18:17', '2017-01-18 23:18:17', '0');
INSERT INTO `tsl_photos` VALUES ('13', 'A0006_front.jpg', '6', '2017-01-18 23:18:18', '2017-01-18 23:18:18', '0');
INSERT INTO `tsl_photos` VALUES ('14', 'A0005_front.jpg', '5', '2017-01-18 23:18:18', '2017-01-18 23:18:18', '0');
INSERT INTO `tsl_photos` VALUES ('15', 'A0008.jpg', '8', '2017-01-18 23:18:18', '2017-01-18 23:18:18', '0');
INSERT INTO `tsl_photos` VALUES ('16', 'A0008_back.jpg', '8', '2017-01-18 23:18:19', '2017-01-18 23:18:19', '0');
INSERT INTO `tsl_photos` VALUES ('17', 'A0008_side_1.jpg', '8', '2017-01-19 23:08:34', '2017-01-19 23:08:34', '0');
INSERT INTO `tsl_photos` VALUES ('18', 'A0009.jpg', '9', '2017-01-19 23:50:46', '2017-01-19 23:50:46', '0');
INSERT INTO `tsl_photos` VALUES ('19', 'A0010_back.jpg', '10', '2017-01-19 23:50:46', '2017-01-19 23:50:46', '0');
INSERT INTO `tsl_photos` VALUES ('20', 'A0010.jpg', '10', '2017-01-19 23:50:47', '2017-01-19 23:50:47', '0');
INSERT INTO `tsl_photos` VALUES ('21', 'A0011.jpg', '0', '2017-01-19 23:50:47', '2017-01-19 23:50:47', '0');
INSERT INTO `tsl_photos` VALUES ('22', 'A0011_back.jpg', '0', '2017-01-19 23:50:48', '2017-01-19 23:50:48', '0');
INSERT INTO `tsl_photos` VALUES ('23', 'A0009_back.jpg', '9', '2017-01-19 23:50:48', '2017-01-19 23:50:48', '0');
INSERT INTO `tsl_photos` VALUES ('24', 'A0011_front.jpg', '0', '2017-01-19 23:50:48', '2017-01-19 23:50:48', '0');
INSERT INTO `tsl_photos` VALUES ('25', 'P0002.jpg', '12', '2017-03-10 09:48:33', '2017-03-10 09:49:07', '0');
INSERT INTO `tsl_photos` VALUES ('26', 'P0001.jpg', '11', '2017-03-10 09:48:33', '2017-03-10 09:48:49', '0');
INSERT INTO `tsl_photos` VALUES ('27', 'P0006.jpg', '16', '2017-03-10 10:21:59', '2017-03-10 10:23:00', '0');
INSERT INTO `tsl_photos` VALUES ('28', 'P0004.jpg', '14', '2017-03-10 10:21:59', '2017-03-10 10:22:27', '0');
INSERT INTO `tsl_photos` VALUES ('29', 'P0008.jpg', '18', '2017-03-10 10:21:59', '2017-03-10 10:23:16', '0');
INSERT INTO `tsl_photos` VALUES ('30', 'P0003.jpg', '13', '2017-03-10 10:21:59', '2017-03-10 10:22:14', '0');
INSERT INTO `tsl_photos` VALUES ('31', 'P0007.jpg', '17', '2017-03-10 10:21:59', '2017-03-10 10:23:08', '0');
INSERT INTO `tsl_photos` VALUES ('32', 'P0005.jpg', '15', '2017-03-10 10:21:59', '2017-03-10 10:22:49', '0');
INSERT INTO `tsl_photos` VALUES ('33', 'A0001.jpg', null, '2017-04-10 10:49:44', '2017-04-10 10:49:44', '0');
INSERT INTO `tsl_photos` VALUES ('34', 'A0003.jpg', null, '2017-04-10 10:49:44', '2017-04-10 10:49:44', '0');
INSERT INTO `tsl_photos` VALUES ('35', 'A0004.jpg', null, '2017-04-10 10:49:44', '2017-04-10 10:49:44', '0');
INSERT INTO `tsl_photos` VALUES ('36', 'A0002.jpg', null, '2017-04-10 10:49:44', '2017-04-10 10:49:44', '0');

-- ----------------------------
-- Table structure for tsl_products
-- ----------------------------
DROP TABLE IF EXISTS `tsl_products`;
CREATE TABLE `tsl_products` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `pro_code` varchar(10) DEFAULT NULL,
  `cate_code` varchar(100) DEFAULT NULL,
  `sup_id` int(5) DEFAULT NULL,
  `color` varchar(20) DEFAULT NULL,
  `import_date` datetime DEFAULT NULL,
  `quantity` int(5) DEFAULT NULL,
  `quality_stt` int(5) DEFAULT NULL,
  `price` double(10,0) DEFAULT NULL,
  `price_up` double(10,0) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `is_sold` int(1) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=76 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of tsl_products
-- ----------------------------
INSERT INTO `tsl_products` VALUES ('1', '0001', 'A', '1', 'Trắng', '2016-07-23 00:00:00', '1', '90', '1500000', '2500000', '2017-03-10 04:33:49', '2017-03-10 04:33:49', '0');
INSERT INTO `tsl_products` VALUES ('2', '0002', 'A', '1', 'Đỏ', '2016-07-23 00:00:00', '1', '90', '1500000', '2500000', '2017-03-10 04:33:49', '2017-03-10 04:33:49', '0');
INSERT INTO `tsl_products` VALUES ('3', '0003', 'A', '1', 'Trắng', '2016-07-23 00:00:00', '1', '95', '650000', '850000', '2017-03-10 04:33:49', '2017-03-10 04:33:49', '0');
INSERT INTO `tsl_products` VALUES ('4', '0004', 'A', '1', 'Trắng', '2016-09-05 00:00:00', '1', '100', '2000000', '4000000', '2017-03-10 04:33:49', '2017-03-10 04:33:49', '0');
INSERT INTO `tsl_products` VALUES ('5', '0005', 'A', '1', 'Trắng', '2016-09-19 00:00:00', '1', '100', '2000000', '4000000', '2017-03-10 04:33:49', '2017-03-10 04:33:49', '0');
INSERT INTO `tsl_products` VALUES ('6', '0006', 'A', '1', 'Trắng', '2016-09-19 00:00:00', '1', '100', '1800000', '2800000', '2017-03-10 04:33:49', '2017-03-10 04:33:49', '0');
INSERT INTO `tsl_products` VALUES ('7', '0007', 'A', '1', 'Hồng dâu', '2016-09-19 00:00:00', '1', '100', '2800000', '3800000', '2017-03-10 04:33:49', '2017-03-10 04:33:49', '0');
INSERT INTO `tsl_products` VALUES ('8', '0008', 'A', '1', 'Trắng', '2016-09-19 00:00:00', '1', '100', '3400000', '4800000', '2017-03-10 04:33:49', '2017-03-10 04:33:49', '0');
INSERT INTO `tsl_products` VALUES ('9', '0009', 'A', '1', 'Vàng gold', '2016-09-24 00:00:00', '1', '100', '1300000', '2800000', '2017-03-10 04:33:49', '2017-03-10 04:33:49', '0');
INSERT INTO `tsl_products` VALUES ('10', '0010', 'A', '1', 'Đỏ hoa hồng', '2016-09-24 00:00:00', '1', '100', '990000', '2600000', '2017-03-10 04:33:49', '2017-03-10 04:33:49', '0');
INSERT INTO `tsl_products` VALUES ('11', '0001', 'P', '1', null, '2016-07-23 00:00:00', '1', '98', '100000', '150000', '2017-03-10 04:33:49', '2017-03-10 04:33:49', '0');
INSERT INTO `tsl_products` VALUES ('12', '0002', 'P', '1', null, '2016-07-23 00:00:00', '1', '98', '70000', '100000', '2017-03-10 04:33:49', '2017-03-10 04:33:49', '0');
INSERT INTO `tsl_products` VALUES ('13', '0003', 'P', '1', null, '2016-07-23 00:00:00', '1', '98', '50000', '70000', '2017-03-10 04:33:49', '2017-03-10 04:33:49', '0');
INSERT INTO `tsl_products` VALUES ('14', '0004', 'P', '1', null, '2016-07-23 00:00:00', '1', '100', '60000', '100000', '2017-03-10 04:33:49', '2017-03-10 04:33:49', '0');
INSERT INTO `tsl_products` VALUES ('15', '0005', 'P', '1', null, '2016-07-23 00:00:00', '1', '100', '0', '280000', '2017-03-10 04:33:49', '2017-03-10 04:33:49', '0');
INSERT INTO `tsl_products` VALUES ('16', '0006', 'P', '1', null, '2016-07-23 00:00:00', '1', '100', '0', '100000', '2017-03-10 04:33:49', '2017-03-10 04:33:49', '0');
INSERT INTO `tsl_products` VALUES ('17', '0007', 'P', '1', null, '2016-09-24 00:00:00', '1', '100', '200000', '300000', '2017-03-10 04:33:49', '2017-03-10 04:33:49', '0');
INSERT INTO `tsl_products` VALUES ('18', '0008', 'P', '1', null, '2016-09-24 00:00:00', '1', '100', '150000', '200000', '2017-03-10 04:33:49', '2017-03-10 04:33:49', '0');
INSERT INTO `tsl_products` VALUES ('19', '0009', 'P', '1', null, '2016-09-24 00:00:00', '1', '100', '140000', '180000', '2017-03-10 04:33:49', '2017-03-10 04:33:49', '0');
INSERT INTO `tsl_products` VALUES ('20', '0010', 'P', '1', null, '2016-09-24 00:00:00', '1', '100', '48000', '70000', '2017-03-10 04:33:49', '2017-03-10 04:33:49', '0');
INSERT INTO `tsl_products` VALUES ('21', '0011', 'P', '1', null, '2016-09-24 00:00:00', '1', '100', '250000', '300000', '2017-03-10 04:33:49', '2017-03-10 04:33:49', '0');
INSERT INTO `tsl_products` VALUES ('22', '0012', 'P', '1', null, '2016-09-24 00:00:00', '1', '100', '50000', '70000', '2017-03-10 04:33:49', '2017-03-10 04:33:49', '0');
INSERT INTO `tsl_products` VALUES ('23', '0013', 'P', '1', null, '2016-09-24 00:00:00', '1', '100', '50000', '70000', '2017-03-10 04:33:49', '2017-03-10 04:33:49', '0');
INSERT INTO `tsl_products` VALUES ('24', '0014', 'P', '1', null, '2016-09-24 00:00:00', '1', '100', '70000', '100000', '2017-03-10 04:33:49', '2017-03-10 04:33:49', '0');
INSERT INTO `tsl_products` VALUES ('25', '0015', 'P', '1', null, '2016-09-24 00:00:00', '1', '100', '110000', '170000', '2017-03-10 04:33:49', '2017-03-10 04:33:49', '0');
INSERT INTO `tsl_products` VALUES ('26', '0016', 'P', '1', null, '2016-09-24 00:00:00', '1', '100', '100000', '250000', '2017-03-10 04:33:49', '2017-03-10 04:33:49', '0');
INSERT INTO `tsl_products` VALUES ('27', '0017', 'P', '1', null, '2016-09-25 00:00:00', '1', '100', '70000', '100000', '2017-03-10 04:33:49', '2017-03-10 04:33:49', '0');
INSERT INTO `tsl_products` VALUES ('28', '0018', 'P', '1', null, '2016-07-23 00:00:00', '1', '98', '0', '120000', '2017-03-10 04:33:49', '2017-03-10 04:33:49', '0');
INSERT INTO `tsl_products` VALUES ('29', '0019', 'P', '1', null, '2016-07-23 00:00:00', '1', '100', '0', '80000', '2017-03-10 04:33:49', '2017-03-10 04:33:49', '0');
INSERT INTO `tsl_products` VALUES ('30', '0020', 'P', '1', null, '2016-07-23 00:00:00', '1', '98', '100000', '150000', '2017-03-10 04:33:49', '2017-03-10 04:33:49', '0');
INSERT INTO `tsl_products` VALUES ('31', '0021', 'P', '1', null, '2016-07-23 00:00:00', '1', '100', '0', '300000', '2017-03-10 04:33:49', '2017-03-10 04:33:49', '0');
INSERT INTO `tsl_products` VALUES ('32', '0022', 'P', '1', null, '2016-07-23 00:00:00', '1', '100', '0', '120000', '2017-03-10 04:33:49', '2017-03-10 04:33:49', '0');
INSERT INTO `tsl_products` VALUES ('33', '0023', 'P', '1', null, '2016-07-23 00:00:00', '1', '98', '90000', '120000', '2017-03-10 04:33:49', '2017-03-10 04:33:49', '0');
INSERT INTO `tsl_products` VALUES ('34', '0024', 'P', '1', null, '2016-07-23 00:00:00', '1', '98', '80000', '110000', '2017-03-10 04:33:49', '2017-03-10 04:33:49', '0');
INSERT INTO `tsl_products` VALUES ('35', '0025', 'P', '1', null, '2016-07-23 00:00:00', '1', '98', '70000', '90000', '2017-03-10 04:33:49', '2017-03-10 04:33:49', '0');
INSERT INTO `tsl_products` VALUES ('36', '0026', 'P', '1', null, '2016-07-23 00:00:00', '1', '95', '0', '150000', '2017-03-10 04:33:49', '2017-03-10 04:33:49', '0');
INSERT INTO `tsl_products` VALUES ('37', '0001', 'D', '1', null, '2016-07-23 00:00:00', '1', '98', '650000', '1000000', '2017-03-10 04:33:49', '2017-03-10 04:33:49', '0');
INSERT INTO `tsl_products` VALUES ('38', '0002', 'D', '1', null, '2016-09-25 00:00:00', '1', '100', '700000', '1250000', '2017-03-10 04:33:49', '2017-03-10 04:33:49', '0');
INSERT INTO `tsl_products` VALUES ('39', '0003', 'D', '1', null, '2016-09-25 00:00:00', '1', '100', '500000', '1000000', '2017-03-10 04:33:49', '2017-03-10 04:33:49', '0');
INSERT INTO `tsl_products` VALUES ('40', '0004', 'D', '1', null, '2016-09-25 00:00:00', '1', '100', '950000', '1350000', '2017-03-10 04:33:49', '2017-03-10 04:33:49', '0');
INSERT INTO `tsl_products` VALUES ('41', '0001', 'DN', '1', null, '2016-07-23 00:00:00', '1', '98', '750000', '1000000', '2017-03-10 04:33:49', '2017-03-10 04:33:49', '0');
INSERT INTO `tsl_products` VALUES ('42', '0002', 'DN', '1', null, '2016-06-25 00:00:00', '1', '100', '500000', '1000000', '2017-03-10 04:33:49', '2017-03-10 04:33:49', '0');
INSERT INTO `tsl_products` VALUES ('43', '0001', 'V', '1', null, '2016-07-23 00:00:00', '1', '98', '600000', '1200000', '2017-03-10 04:33:49', '2017-03-10 04:33:49', '0');
INSERT INTO `tsl_products` VALUES ('44', '0002', 'V', '1', null, '2016-07-23 00:00:00', '1', '98', '700000', '1100000', '2017-03-10 04:33:49', '2017-03-10 04:33:49', '0');
INSERT INTO `tsl_products` VALUES ('45', '0003', 'V', '1', null, '2016-09-25 00:00:00', '1', '100', '500000', '1000000', '2017-03-10 04:33:49', '2017-03-10 04:33:49', '0');
INSERT INTO `tsl_products` VALUES ('46', '0004', 'V', '1', null, '2016-09-25 00:00:00', '1', '100', '500000', '1000000', '2017-03-10 04:33:49', '2017-03-10 04:33:49', '0');
INSERT INTO `tsl_products` VALUES ('47', '0005', 'D', '1', null, '2016-09-26 00:00:00', '1', '100', '900000', '1300000', '2017-03-10 04:33:49', '2017-03-10 04:33:49', '0');
INSERT INTO `tsl_products` VALUES ('48', '0011', 'A', '1', null, '2016-09-28 00:00:00', '1', '100', '1350000', '3800000', '2017-03-10 04:33:49', '2017-03-10 04:33:49', '0');
INSERT INTO `tsl_products` VALUES ('49', '0027', 'P', '1', null, '2016-09-28 00:00:00', '1', '100', '150000', '250000', '2017-03-10 04:33:49', '2017-03-10 04:33:49', '0');
INSERT INTO `tsl_products` VALUES ('50', '0028', 'P', '1', null, '2016-09-28 00:00:00', '1', '100', '50000', '150000', '2017-03-10 04:33:49', '2017-03-10 04:33:49', '0');
INSERT INTO `tsl_products` VALUES ('51', '0029', 'P', '1', null, '2016-09-28 00:00:00', '1', '100', '60000', '130000', '2017-03-10 04:33:49', '2017-03-10 04:33:49', '0');
INSERT INTO `tsl_products` VALUES ('52', '0030', 'P', '1', null, '2016-09-28 00:00:00', '1', '100', '80000', '130000', '2017-03-10 04:33:49', '2017-03-10 04:33:49', '0');
INSERT INTO `tsl_products` VALUES ('53', '0031', 'P', '1', null, '2016-09-28 00:00:00', '1', '100', '65000', '150000', '2017-03-10 04:33:49', '2017-03-10 04:33:49', '0');
INSERT INTO `tsl_products` VALUES ('54', '0006', 'D', '1', null, '2016-09-28 00:00:00', '1', '100', '900000', '1100000', '2017-03-10 04:33:49', '2017-03-10 04:33:49', '0');
INSERT INTO `tsl_products` VALUES ('55', '0032', 'P', '1', null, '2016-07-23 00:00:00', '1', '98', '0', '120000', '2017-03-10 04:33:49', '2017-03-10 04:33:49', '0');
INSERT INTO `tsl_products` VALUES ('56', '0033', 'P', '1', null, '2016-07-23 00:00:00', '1', '98', '0', '120000', '2017-03-10 04:33:49', '2017-03-10 04:33:49', '0');
INSERT INTO `tsl_products` VALUES ('57', '0034', 'P', '1', null, '2016-07-23 00:00:00', '1', '98', '110000', '120000', '2017-03-10 04:33:49', '2017-03-10 04:33:49', '0');
INSERT INTO `tsl_products` VALUES ('58', '0035', 'P', '1', null, '2016-07-23 00:00:00', '1', '98', '0', '90000', '2017-03-10 04:33:49', '2017-03-10 04:33:49', '0');
INSERT INTO `tsl_products` VALUES ('59', '0036', 'P', '1', null, '2016-07-23 00:00:00', '1', '98', '150000', '160000', '2017-03-10 04:33:49', '2017-03-10 04:33:49', '0');
INSERT INTO `tsl_products` VALUES ('60', '0037', 'P', '1', null, '2016-07-23 00:00:00', '1', '98', '150000', '160000', '2017-03-10 04:33:49', '2017-03-10 04:33:49', '0');
INSERT INTO `tsl_products` VALUES ('61', '0007', 'D', '1', null, '2016-10-02 00:00:00', '1', '100', '600000', '1100000', '2017-03-10 04:33:49', '2017-03-10 04:33:49', '0');
INSERT INTO `tsl_products` VALUES ('62', '0008', 'D', '1', null, '2016-10-02 00:00:00', '1', '100', '750000', '1250000', '2017-03-10 04:33:49', '2017-03-10 04:33:49', '0');
INSERT INTO `tsl_products` VALUES ('63', '0003', 'DN', '1', null, '2016-11-16 00:00:00', '1', '100', '600000', '1100000', '2017-03-10 04:33:49', '2017-03-10 04:33:49', '0');
INSERT INTO `tsl_products` VALUES ('64', '0004', 'DN', '1', null, '2016-10-02 00:00:00', '1', '100', '800000', '1250000', '2017-03-10 04:33:49', '2017-03-10 04:33:49', '0');
INSERT INTO `tsl_products` VALUES ('65', '0001', 'Q', '1', null, '2016-07-23 00:00:00', '1', '98', '0', '400000', '2017-03-10 04:33:49', '2017-03-10 04:33:49', '0');
INSERT INTO `tsl_products` VALUES ('66', '0002', 'Q', '1', null, '2016-10-04 00:00:00', '1', '100', '0', '450000', '2017-03-10 04:33:49', '2017-03-10 04:33:49', '0');
INSERT INTO `tsl_products` VALUES ('67', '0003', 'Q', '1', null, '2016-10-04 00:00:00', '1', '100', '0', '450000', '2017-03-10 04:33:49', '2017-03-10 04:33:49', '0');
INSERT INTO `tsl_products` VALUES ('68', '0004', 'Q', '1', null, '2016-10-04 00:00:00', '1', '100', '0', '450000', '2017-03-10 04:33:49', '2017-03-10 04:33:49', '0');
INSERT INTO `tsl_products` VALUES ('69', '0005', 'Q', '1', null, '2016-10-04 00:00:00', '1', '100', '0', '450000', '2017-03-10 04:33:49', '2017-03-10 04:33:49', '0');
INSERT INTO `tsl_products` VALUES ('70', '0001', 'QN', '1', null, '2016-10-04 00:00:00', '1', '100', '0', '500000', '2017-03-10 04:33:49', '2017-03-10 04:33:49', '0');
INSERT INTO `tsl_products` VALUES ('71', '0002', 'QN', '1', null, '2016-10-05 00:00:00', '1', '100', '0', '500000', '2017-03-10 04:33:49', '2017-03-10 04:33:49', '0');
INSERT INTO `tsl_products` VALUES ('72', '0005', 'V', '1', null, '2016-10-08 00:00:00', '1', '100', '500000', '1100000', '2017-03-10 04:33:49', '2017-03-10 04:33:49', '0');
INSERT INTO `tsl_products` VALUES ('73', '0006', 'V', '1', null, '2016-10-08 00:00:00', '1', '100', '500000', '1100000', '2017-03-10 04:33:49', '2017-03-10 04:33:49', '0');
INSERT INTO `tsl_products` VALUES ('74', '0007', 'V', '1', null, '2016-10-08 00:00:00', '1', '100', '450000', '1100000', '2017-03-10 04:33:49', '2017-03-10 04:33:49', '0');
INSERT INTO `tsl_products` VALUES ('75', '0005', 'DN', '1', null, '2016-11-16 00:00:00', '1', '100', '550000', null, '2017-03-10 04:33:49', '2017-03-10 04:33:49', '0');

-- ----------------------------
-- Table structure for tsl_suppliers
-- ----------------------------
DROP TABLE IF EXISTS `tsl_suppliers`;
CREATE TABLE `tsl_suppliers` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `sup_name` varchar(255) DEFAULT NULL,
  `sup_phone` varchar(255) DEFAULT NULL,
  `sup_address` varchar(255) DEFAULT NULL,
  `sup_website` varchar(255) DEFAULT NULL,
  `bank_info` text,
  `map` varchar(50) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `is_delete` int(1) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of tsl_suppliers
-- ----------------------------
INSERT INTO `tsl_suppliers` VALUES ('1', 'Hiệp Mỹ', '0903055944', 'Chợ Tân Bình', 'https://www.facebook.com/chhotanbinh/', 'Ngân hàng Vietinbank\nSố TK: 0123458758\nChi nhánh Tp.HCM', '10.78583, 106.65244', '2016-10-06 21:48:05', '2017-02-28 07:30:41', '0');
INSERT INTO `tsl_suppliers` VALUES ('2', 'Áo cưới đẹp', '01238888001', '1A Cao Thắng', 'https://www.facebook.com/shopaocuoidepgiasi/', 'Ngân hàng Vietcombank chi nhánh Sài Gòn', '10.768405, 106.683826', '2016-10-06 22:56:14', '2016-10-09 20:06:36', '0');
INSERT INTO `tsl_suppliers` VALUES ('3', 'MEOW Wedding', '0866819800', '7A/54 Thành Thái, P 14, Q 10 ( đi cuối hẻm 51 Thành Thái quẹo trái ) Ho Chi Minh City, Vietnam', 'https://www.facebook.com/MeowWedding/?fref=ts', '', '10.770782, 106.662537', '2016-10-10 23:06:07', '2017-01-18 23:08:31', '0');
INSERT INTO `tsl_suppliers` VALUES ('4', 'Cửa hàng trên Facebook', '0906099303', 'Facebook', '', '', '10.777118, 106.695419', '2017-01-18 23:26:56', '2017-02-23 03:59:07', '0');
INSERT INTO `tsl_suppliers` VALUES ('5', 'Fly fashion', '0902715157', 'Facebook', 'https://www.facebook.com/FlyFashionTA/?fref=ts', '', '10.777118, 106.695419', '2017-01-18 23:29:17', '2017-01-18 23:27:29', '0');
INSERT INTO `tsl_suppliers` VALUES ('6', 'Xưởng áo cưới Tân Bình', '', 'Tân Bình, Tp.Hồ Chí Minh', '', '', '10.777118, 106.695419', '2017-01-18 23:41:59', '2017-01-18 23:27:29', '0');

$(document).ready(function()
{

  //Create datapicker for input date
  $( function() {
    $( "#rent_date, #back_date" ).datepicker({
      dateFormat: "dd/mm/yy",
      changeMonth: true,
      changeYear: true,
      yearRange: ((new Date).getFullYear()-5) + ':' + ((new Date).getFullYear()),
      maxDate: new Date
    });
  });

});//end ready
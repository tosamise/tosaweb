/**
** Class Price to do operator to calculate the product price.
** file: /webroot/js/price.js
**
** Author: HuyDo
** Date: Nov 2016
** Ver: 1.0
**
**/
function Price()
{
    //Variables
    this.origin_price = '';
    this.quality = '';
     
    //Functions:
    
	/**
	 * Description: Set global value
	 * Function: setInfo
	 * @author: HuyDo
	 * @params: None
	 * @return: None
	 * @version: 1.0
	 */
    this.setInfo = function(origin_price,quality)
    {
        this.origin_price = origin_price;
        this.quality = quality;
    };
    
    
	/**
	 * Description: Calculate the rent price from origin price
	 * Function: calRentPrice
	 * @author: HuyDo
	 * @params: None
	 * @return: Int: sellprice
	 * @version: 1.0
	 */
    this.calRentPrice = function(){
        return this.origin_price * 30 / 100;
    };

    /**
	 * Description: Calculate the sell price from origin price
	 * Function: calSellPrice
	 * @author: HuyDo
	 * @params: None
	 * @return: Int: sellprice
	 * @version: 1.0
	 */
    this.calSellPrice = function(){
    	if(this.quality >= 90)
    	{
        	return this.origin_price;
        }
        else
        {
        	return this.origin_price * 80 / 100;
        }
    };
     
   
     
    return this;
}

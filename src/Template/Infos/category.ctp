<?= $this->Html->css('jquery.dataTables.min.css'); ?>
<?= $this->Html->css('magnific-popup.css'); ?>

<?= $this->Form->input('search',['class'=>'form-control mb20 w300',
                                 'id'=>'search_box',
                                 'label'=>false,
                                 'placeholder'=>'Nhập để tìm kiếm',
                                 'templates' => [
                                    'inputContainer' => '<div class="display-inbl">{{content}}</div>'
                                  ],
                                ]); ?>

<a href='#add_cate_popup' class='btn btn-default pb40' id="add_cate_btn"><i class="fa fa-plus"></i> Thêm danh mục</a>

<table id="category_table" class="table table-hover stripe">
  <thead>
  <tr>
    <th>STT</th>
    <th>Mã danh mục</th>
    <th>Tên danh mục</th>
    <th>Ngày tạo</th>
    <th>Lần cuối chỉnh sửa</th>
    <th>ID</th>
  </tr>
  </thead>
  <tbody>
  <?php $no = "1" ?>
  <?php foreach($categories as $category): ?>
  <tr>
    <td class="cate_id"><?= $no ?></td>
    <td class=""><?= $category->cate_code ?></td>
    <td class="editable cate_name"><?= $category->cate_name ?></td>
    <td class=""><?= $category->created ?></td>
    <td class=""><?= $category->modified ?></td>
    <td><?= $category->id ?></td>
    <?php $no++ ?>
  </tr>
  <?php endforeach; ?>
  </tbody>
</table>

<!-- Popup add category -->
<div id="add_cate_popup" class="custom_popup w50p mfp-hide white-popup-block" style="border: 3px solid #f39c12;">
  <div class="box-body">
    <div class="row">
      <div class="col-md-12">
        <div class="form-group">
        <?= $this->Form->create(null, ['id'=>'add_cate_form']) ?>
          <div class="input-group">
            <div class="input-group-addon">
                <i class="fa fa-code"></i>
            </div>
            <?= $this->Form->input('cate_code', ['class'=>'form-control','id'=>'cate_code','placeholder'=>'Mã danh mục','div'=>false,'label'=>'','error'=>false] ); ?>
          </div>
          <?= $this->Form->error('cate_code', null, array('class' => 'error-message','div'=>false)); ?>

          <div class="input-group mt10">
            <div class="input-group-addon">
                <i class="fa fa-font"></i>
            </div>
            <?= $this->Form->input('cate_name', ['class'=>'form-control','id'=>'cate_name','placeholder'=>'Tên danh mục','div'=>false,'label'=>'','error'=>false] ); ?>
          </div>
          <?= $this->Form->error('cate_name', null, array('class' => 'error-message','div'=>false)); ?>
          <?= $this->Form->end() ?>
        </div>
        <!-- end form-group -->
      </div>
    </div>
    <div class="row">
      <div class="col-md-12" style="margin-bottom: -70px;">
        <button class='back_btn btn btn-app fr pb40'><i class="fa fa-undo"></i> Quay lại</button>
        <button class='save_btn btn btn-app fr pb40 mr20' id="submit_cate_btn"><i class="fa fa-save"></i> Lưu</button>
      </div>
    </div>
  </div>
</div>

<?= $this->Html->script('jquery.dataTables.min.js'); ?>
<?= $this->Html->script('jquery.magnific-popup.min.js'); ?>

<script type="text/javascript">
$(document).ready(function(){

	//Create the datatable
  var oTable  = $('#category_table').DataTable({
    "oLanguage": {
      "oPaginate": {
        "sFirst":    "Trang đầu",
        "sLast":    "Trang cuối",
        "sNext":    "Trang kế",
        "sPrevious": "Trang trước"
      },
      "sInfo": "Hiển thị _START_ đến _END_ trên tổng _TOTAL_ records",
      "sInfoEmpty": "Hiển thị 0 dữ liệu",
      "sZeroRecords": "Không tìm thấy dữ liệu",
    },
    "bLengthChange": false, //Hide select box
    "columnDefs": [
      { className: "dt-center", targets: [0,1,3,4] },
      {
        targets: -1,
       	visible: false,
        render: function(row, type, val, meta) {
          return val[5];
        }
      },
      {
        targets: 0,
        render: function(row, type, val, meta) {
          return val[0]+"<input class='cate_id_input' type='hidden' value='"+val[5]+"'/>";
        }
      },
      {
        targets: -3,
        render: function(row, type, val, meta) {
          var date = new Date( val[3] );
          var day = date.getDate();
          if(day<10){ day = "0"+day; }
          var month = (date.getMonth()+1);
          if(month<10){ month = "0"+month; }
          return day+"/"+month+"/"+date.getFullYear();
        }
      },
      {
        targets: -2,
        render: function(row, type, val, meta) {
          var date = new Date( val[4] );
          var day = date.getDate();
          if(day<10){ day = "0"+day; }
          var month = (date.getMonth()+1);
          if(month<10){ month = "0"+month; }
          return day+"/"+month+"/"+date.getFullYear();
        }
      },
    ]
  });

  //Action when search is input.
  $("#search_box").keyup(function(){
    oTable.search($(this).val()).draw() ;
  })

  //Quick edit data in table
  var cur_data = "";
  
  $("#category_table").on("dblclick", ".editable" , function(e){
  	if( $("#category_table").find("input#qk_input").length < 1 )
    {
    	cur_data = $(this).text();
    	var input = "<input class='w150 text-r' id='qk_input' value='"+cur_data+"' />";

	  	var html_content = input+"<i class='fa fa-check success ml5 cur-point' id='qk_ok' title='Lưu nhanh'></i><i class='fa fa-ban error ml5 cur-point' id='qk_cancel' title='Hủy'></i>";

	  	//Execute html content to view
	    $(this).html(html_content);
	    $("#qk_input").focus().select();
  	}
  });

  //Click to close quick editor
  $('.editable').on('click', '#qk_cancel', function(){
  	$(this).parent().text(cur_data);
    $(this).prevAll('input').remove();
  });

  //Click to close quick editor and save data
  $('.editable').on('click', '#qk_ok', function(){

  	var edited_id = $(this).parent().prevAll(".cate_id").children(".cate_id_input").val();
  	var old_data = cur_data;
  	var edited_data = $(this).prevAll('#qk_input').val();

  	//Call ajax to save this data
  	$('#divLoading').addClass('show');
    $.ajax({
      type: "POST",
      url: "<?php echo $this->Url->build( [ 'action' => 'ajax' ] ) ?>",
      data: {
      			 quick_update_cate: edited_id, 
      			 edited_data: edited_data, 
      			 old_data : old_data 
      			},
      success: function(data){
        obj = jQuery.parseJSON(data);
        if(obj['data'] == false){
          swal("Đã xảy ra lỗi, hãy thử lại");
        }
      },
      complete: function(){
        $('#divLoading').removeClass('show');
      },
      error: function(){
        swal("Đã xảy ra lỗi, hãy thử lại");
        location.reload();
      }
    });//end ajax

    $(this).parent().text(edited_data);
  	$(this).prevAll('input').remove();
  });

  //Initialize the magnific popup add category
  $(function () {
    $('#add_cate_btn').magnificPopup({
      type: 'inline',
      preloader: false,
      focus: '#add_cate_popup',
      closeBtnInside:true,
      modal: true
    });
    $(document).on('click', '.back_btn', function (e) {
      e.preventDefault();
      $.magnificPopup.close();
    });
  });//end function

    $("#submit_cate_btn").on('click',function(){
    //Get data
    var data = {
      'cate_code' : $("#cate_code").val(),
      'cate_name' : $("#cate_name").val(),
    };
    if( $("#add_cate_form").valid() )
    {
      swal({
        title: "Bạn chắc chắn chưa?",
        text: "Danh mục mới sẽ được thêm vào hệ thống",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#008d4c",
        confirmButtonText: "Thêm vào",
        cancelButtonText: "Không, đợi tý",
        closeOnConfirm: true
      }, function(isConfirm){
        if(isConfirm)
        {
        	$('#divLoading').addClass('show');
          $.ajax({
            type: "POST",
            url: "<?php echo $this->Url->build(['controller'=>'MProducts', 'action'=>'ajax']) ?>",
            data: {qk_add_cate: data},
            //async: false,
            success: function(data){
              var Obj = jQuery.parseJSON(data);
              if(Obj != "")
              {
                location.reload();
              }
            },
            complete: function(){
              $('#divLoading').removeClass('show');
            },
            error: function(){
              $('#divLoading').removeClass('show');
              swal("Đã xảy ra lỗi, hãy thử lại");
            }
          });//end ajax
        }
      });
    }
  });

  //Form validation
  $("#add_cate_form").validate({
    rules: {
      cate_code: {
        required: true,
        requiredCaps: true,
        maxlength: 4,
      },
      cate_name: "required",
    },
    messages: {
      cate_code: {
        required: "Ô này cần được nhập thông tin",
        requiredCaps: "Chỉ sử dụng chữ IN HOA không dấu nhé",
        maxlength: "Mã danh mục dài quá, 4 chữ thôi nhé"
      },
      cate_name: "Ô này cần được nhập thông tin",
    },
    highlight: function(element) {
      $("#submit_cate_btn").attr('disabled',true);
      $(element).closest('.input-group').addClass('has-error');
    },
    unhighlight: function(element) {
      $("#submit_cate_btn").attr('disabled',false);
      $(element).closest('.input-group').removeClass('has-error');
    },
    errorElement: 'span',
    errorClass: 'help-block error',
    errorPlacement: function(error, element) {
        if(element.parents('.input-group').length) {
            error.insertAfter(element.parents(".input-group"));
        } else {
            error.insertAfter(element);
        }
      }
  }); 

  //Auto capitalize word when typing
  $("#cate_code").on('keyup change',function(){
    $(this).val( $(this).val().toUpperCase() );
  })

  //Check pro_code existed in add category popup
  $("#cate_code").on('change',function(){
    if( $(this).val() != "" )
    {
    	$('#divLoading').addClass('show');
      $.ajax({
        type: "POST",
        url: "<?php echo $this->Url->build(['controller'=>'MProducts', 'action'=>'ajax']) ?>",
        data: {checkCatecode: $(this).val() },
        success: function(data){
          obj = jQuery.parseJSON(data);
          if(!obj["data"])
          {
            swal("Mã sản phẩm này đã có rồi !!!");
            $("#cate_code").val("").focus();
          }
        },
        complete: function(){
          $('#divLoading').removeClass('show');
        },
        error: function(){
          swal("Đã xảy ra lỗi, hãy thử lại");
          $('#divLoading').removeClass('show');
        }
      });//end ajax
    }
  });
	

});	
</script>
<?= $this->Html->css('jquery.dataTables.min.css'); ?>
<?= $this->Html->css('magnific-popup.css'); ?>
<?= $this->Html->css('leaflet.css') ?>
<style type="text/css">
  #mapid, #add_mapid { height: 370px; }
</style>

<?= $this->Form->input('search',['class'=>'form-control mb20 w300',
                                 'id'=>'search_box',
                                 'label'=>false,
                                 'placeholder'=>'Nhập để tìm kiếm',
                                 'templates' => [
                                    'inputContainer' => '<div class="display-inbl">{{content}}</div>'
                                  ],
                                ]); ?>

<a href='#add_sup_popup' class='btn btn-default pb40' id="add_sup_btn"><i class="fa fa-plus"></i> Thêm danh mục</a>

<table id="supplier_table" class="table table-hover stripe">
  <thead>
  <tr>
    <th>STT</th>
    <th>Tên nhà cung cấp</th>
    <th>Số điện thoại</th>
    <th>Website</th>
    <th>Ngày tạo</th>
    <th>Lần cuối chỉnh sửa</th>
    <th>ID</th>
  </tr>
  </thead>
  <tbody>
  <?php $no = "1" ?>
  <?php foreach($suppliers as $supplier): ?>
  <tr>
    <td class="sup_id"><?= $no ?></td>
    <td class=""><?= $supplier->sup_name ?></td>
    <td class="editable sup_phone"><?= ($supplier->sup_phone?$supplier->sup_phone:"-") ?></td>
    <td class=""><?= $supplier->sup_website ?></td>
    <td class=""><?= $supplier->created ?></td>
    <td class=""><?= $supplier->modified ?></td>
    <td><?= $supplier->id ?></td>
    <?php $no++ ?>
  </tr>
  <?php endforeach; ?>
  </tbody>
</table>

<!-- Popup edit -->
<div id="view_detail_popup" class="custom_popup w50p mfp-hide white-popup-block" style="border: 3px solid #f39c12;">
  <div class="box-body">
  	<!-- View detail -->
    <div class="row" id="show_form">
      <div class="col-md-12 mb30">
      	<h3 id="view_sup_name"></h3>
     		<div class="col-md-4"><b>Điện thoại:</b> </div>
     		<div class="col-md-8"><span id="view_sup_phone"></span></div>
     		<div class="col-md-4"><b>Địa chỉ:</b> </div>
     		<div class="col-md-8"><span id="view_sup_address"></span></div>
     		<div class="col-md-4"><b>Trang chủ:</b> </div>
     		<div class="col-md-8"><span id="view_sup_website"></span></div>
     		<div class="col-md-4"><b>Thông tin ngân hàng:</b> </div>
     		<div class="col-md-8"><span id="view_sup_bankinfo"></span></div>
     		<div class="clr"></div>
      </div>
    </div>
    <!-- Edit popup -->
    <div class="row">
      <div class="col-md-12 mb10">
        <form id="edit_form" >
          <div class="col-md-4"><b>Tên nhà cung cấp:</b></div>
          <div class="col-md-8"><input class="form-control" type="text" name="sup_name" id="edit_sup_name" /></div>
          <div class="col-md-4"><b>Điện thoại:</b> </div>
          <div class="col-md-8"><input class="form-control" type="text" name="sup_phone" id="edit_sup_phone" /></div>
          <div class="col-md-4"><b>Địa chỉ:</b> </div>
          <div class="col-md-8"><input class="form-control" type="text" name="sup_address" id="edit_sup_address" /></div>
          <div class="col-md-4"><b>Trang chủ:</b> </div>
          <div class="col-md-8"><input class="form-control" type="text" name="sup_website" id="edit_sup_website" /></div>
          <div class="col-md-4"><b>Thông tin ngân hàng:</b> </div>
          <div class="col-md-8"><textarea class="form-control" id="edit_bankinfo" ></textarea></div>
          <div class="clr"></div>
          <input type="hidden" id="edit_coordinates" value="" />
          <input type="hidden" id="edit_id" value="" />
        </form>
      </div>
    </div>
    <!-- Map -->
    <div class="row" id="map_popup">
      <i class="ml20">Click vào bản đồ để thay đổi tọa độ</i>
      <div class="col-md-12 mb30" id="map-area"></div>
    </div>

  	<button class='close_btn btn btn-app fr pb40'><i class="fa fa-times"></i> Đóng</button>
    <button class='btn btn-app btn-warning fr pb40 mr20' id="edit_btn"><i class="fa fa-pencil"></i> Chỉnh sửa</button>
    <button class='btn btn-app fr pb40 mr20' id="save_btn"><i class="fa fa-save"></i> Lưu</button>
		 
  </div>
  <!-- box-body -->
</div>

<!-- Popup add -->
<div id="add_sup_popup" class="custom_popup w50p mfp-hide white-popup-block" style="border: 3px solid #f39c12;">
  <div class="box-body">
    <div class="row">
      <div class="col-md-12 mb10">
        <form id="add_form" >
          <div class="col-md-4"><b>Tên nhà cung cấp:</b></div>
          <div class="col-md-8"><input class="form-control" type="text" name="sup_name" id="add_sup_name" /></div>
          <div class="col-md-4"><b>Điện thoại:</b> </div>
          <div class="col-md-8"><input class="form-control" type="text" name="sup_phone" id="add_sup_phone" /></div>
          <div class="col-md-4"><b>Địa chỉ:</b> </div>
          <div class="col-md-8"><input class="form-control" type="text" name="sup_address" id="add_sup_address" /></div>
          <div class="col-md-4"><b>Trang chủ:</b> </div>
          <div class="col-md-8"><input class="form-control" type="text" name="sup_website" id="add_sup_website" /></div>
          <div class="col-md-4"><b>Thông tin ngân hàng:</b> </div>
          <div class="col-md-8"><textarea class="form-control" id="add_bankinfo" ></textarea></div>
          <div class="clr"></div>
          <input type="hidden" id="add_coordinates" value="10.777118, 106.695419" />
        </form>
      </div>
    </div>
    <!-- Map -->
    <div class="row" id="add_map_popup">
      <i class="ml20">Click vào bản đồ để chọn tọa độ</i>
      <div class="col-md-12 mb30" id="add-map-area"></div>
    </div>

    <button class='close_btn btn btn-app fr pb40'><i class="fa fa-times"></i> Đóng</button>
    <button class='save_btn btn btn-app fr pb40 mr20' id="add_btn"><i class="fa fa-save"></i> Lưu</button>
     
  </div>
  <!-- box-body -->
</div>

<?= $this->Html->script('jquery.dataTables.min.js'); ?>
<?= $this->Html->script('jquery.magnific-popup.min.js'); ?>
<?= $this->Html->script('leaflet.js') ?>

<script type="text/javascript">
$(document).ready(function(){

var mymap = "";

//Create the datatable
var oTable  = $('#supplier_table').DataTable({
  "oLanguage": {
    "oPaginate": {
      "sFirst":    "Trang đầu",
      "sLast":    "Trang cuối",
      "sNext":    "Trang kế",
      "sPrevious": "Trang trước"
    },
    "sInfo": "Hiển thị _START_ đến _END_ trên tổng _TOTAL_ records",
    "sInfoEmpty": "Hiển thị 0 dữ liệu",
    "sZeroRecords": "Không tìm thấy dữ liệu",
  },
  "bLengthChange": false, //Hide select box
  "columnDefs": [
    { className: "dt-center", targets: [0,2,4,5,6] },
    {
      targets: -1,
     	visible: false,
      render: function(row, type, val, meta) {
        return val[4];
      }
    },
    {
      targets: 3,
     	visible: false,
      render: function(row, type, val, meta) {
        return val[3];
      }
    },
    {
      targets: 4,
      render: function(row, type, val, meta) {
        var date = new Date( val[4] );
        var day = date.getDate();
        if(day<10){ day = "0"+day; }
        var month = (date.getMonth()+1);
        if(month<10){ month = "0"+month; }
        return day+"/"+month+"/"+date.getFullYear();
      }
    },
    {
      targets: 5,
      render: function(row, type, val, meta) {
        var date = new Date( val[5] );
        var day = date.getDate();
        if(day<10){ day = "0"+day; }
        var month = (date.getMonth()+1);
        if(month<10){ month = "0"+month; }
        return day+"/"+month+"/"+date.getFullYear();
      }
    },
    {
      targets: 0,
      render: function(row, type, val, meta) {
        return val[0]+"<input class='sup_id_input' type='hidden' value='"+val[6]+"'/>";
      }
    },
    {
      targets: 1,
      render: function(row, type, val, meta) {
        return "<a href='javascript: void(0)' class='view_detail_btn' id='"+val[6]+"'>"+val[1]+"</a>";
      }
    },
  ]
});

//Action when search is input.
$("#search_box").keyup(function(){
  oTable.search($(this).val()).draw() ;
})

//Quick edit data in table
var cur_data = "";

$("#supplier_table").on("dblclick", ".editable" , function(e){
	if( $("#supplier_table").find("input#qk_input").length < 1 )
  {
  	cur_data = $(this).text();
  	var input = "<input class='w150 text-r' id='qk_input' value='"+cur_data+"' />";

  	var html_content = input+"<i class='fa fa-check success ml5 cur-point' id='qk_ok' title='Lưu nhanh'></i><i class='fa fa-ban error ml5 cur-point' id='qk_cancel' title='Hủy'></i>";

  	//Execute html content to view
    $(this).html(html_content);
    $("#qk_input").focus().select();
	}
});

//Click to close quick editor
$('.editable').on('click', '#qk_cancel', function(){
	$(this).parent().text(cur_data);
  $(this).prevAll('input').remove();
});

//Click to close quick editor and save data
$('.editable').on('click', '#qk_ok', function(){

	var edited_id = $(this).parent().prevAll(".sup_id").children(".sup_id_input").val();
	var old_data = cur_data;
	var edited_data = $(this).prevAll('#qk_input').val();

	//Call ajax to save this data
	$('#divLoading').addClass('show');
  $.ajax({
    type: "POST",
    url: "<?php echo $this->Url->build( [ 'action' => 'ajax' ] ) ?>",
    data: {
    			 quick_update_sup: edited_id, 
    			 edited_data: edited_data, 
    			 old_data : old_data 
    			},
    success: function(data){
      obj = jQuery.parseJSON(data);
      if(obj['data'] == false){
        swal("Đã xảy ra lỗi, hãy thử lại");
      }
    },
    complete: function(){
      $('#divLoading').removeClass('show');
    },
    error: function(){
      swal("Đã xảy ra lỗi, hãy thử lại");
      location.reload();
    }
  });//end ajax

  $(this).parent().text(edited_data);
	$(this).prevAll('input').remove();
});

// Initialize the magnific popup add supplier
$('#add_sup_btn').magnificPopup({
  //type: 'inline',
  preloader: false,
  focus: '#add_sup_popup',
  closeBtnInside:true,
  modal: true,
  callbacks: {
    open: function () {
      $("#add-map-area").html("<div id='add_mapid'></div>");
      add_map = L.map('add_mapid').setView([10.777118, 106.695419], 13);
      L.tileLayer('https://api.mapbox.com/styles/v1/tosamise/ciznlvl9c006y2sqjw8dh0xzv/tiles/256/{z}/{x}/{y}?access_token=pk.eyJ1IjoidG9zYW1pc2UiLCJhIjoiY2l6bmxxcjNiMDJtcjJxbzhheW1vemp6bCJ9.aZVIuaxMXKFop4CMStehUQ', {
          maxZoom: 24,
      }).addTo(add_map);
      var marker = L.marker([10.777118, 106.695419]).addTo(add_map);

      //Get action click to map
      function onMapClick(e) {
        var coor = e.latlng.toString();
        var coor_val = coor.substring( coor.indexOf("(")+1 , coor.indexOf(")") );
        lat = coor_val.substring(0, coor_val.indexOf(","));
        long = coor_val.substring(coor_val.indexOf(",")+1);
        // console.log(coor_val);
        $("#add_coordinates").val( coor_val );
        //Check if existed markers, remove it.
        if (marker) {
          add_map.removeLayer(marker); // remove
        }
        //Add new marker to map on each click
        marker = L.marker([lat, long]).addTo(add_map);
      }
      add_map.on('click', onMapClick);
    },
  },
});

//Handler for close popup button
$(document).on('click', '.close_btn', function (e) {
  e.preventDefault();
  $.magnificPopup.close();
  mymap.remove();
});

//Action when click to view detail
$(".view_detail_btn").on('click',function(){
  $.ajax({
    type: 'POST',
    url: '<?php echo $this->Url->build( [ 'action' => 'ajax' ] ) ?>',
    data: { 'view_sup_detail': $(this).attr('id') },
    async: false,
    dataType: 'json',
    success: function(result){
      var Obj = result.data[0];
      // console.log(Obj);
      if(Obj != "")
      {
      	//Empty all field in popup
      	$("#view_sup_name").text("");
      	$("#view_sup_phone").text("");
      	$("#view_sup_address").text("");
      	$("#view_sup_website").text("");
      	$("#view_sup_bankinfo").text("");
        //Edit popup
        $("#edit_id").val("");
        $("#edit_sup_name").val("");
        $("#edit_sup_phone").val("");
        $("#edit_sup_address").val("");
        $("#edit_sup_website").val("");
        $("#edit_bankinfo").val("");
        $('#edit_coordinates').val("");
        //Show/Hide content what onload
        $("#edit_form, #save_btn").hide();
        $("#show_form").show();

      	//Set data to field
				$("#view_sup_name").text( Obj.sup_name );
				$("#view_sup_phone").text( (Obj.sup_phone?Obj.sup_phone:"-") );
      	$("#view_sup_address").text( (Obj.sup_address?Obj.sup_address:"-") );
        var website = (Obj.sup_website?Obj.sup_website:"-");
      	$("#view_sup_website").html( "<a href='"+website+"'>"+website+"</a>" );
      	$("#view_sup_bankinfo").html( (Obj.bank_info?Obj.bank_info.replace(/\n/g,"<br>"):"-") );
        //Set data to edit popup
        $("#edit_id").val( Obj.id );
        $("#edit_sup_name").val( Obj.sup_name );
        $("#edit_sup_phone").val( Obj.sup_phone );
        $("#edit_sup_address").val( Obj.sup_address );
        $("#edit_sup_website").val( Obj.sup_website );
        $("#edit_bankinfo").val( Obj.bank_info );
        $('#edit_coordinates').val( Obj.map );

        $.magnificPopup.open({
          type: 'inline',
          preloader: false,
          modal: true,
          items: {
              src: $('#view_detail_popup')
          },
          closeBtnInside: false,
          closeOnBgClick : false,
          enableEscapeKey: false,
          callbacks: {
            open: function () {
              $("#map-area").html("<div id='mapid'></div>");
              lat = Obj.map.substring(0, Obj.map.indexOf(","));
              long = Obj.map.substring(Obj.map.indexOf(",")+1);
              mymap = L.map('mapid').setView([lat, long], 15);
              L.tileLayer('https://api.mapbox.com/styles/v1/tosamise/ciznlvl9c006y2sqjw8dh0xzv/tiles/256/{z}/{x}/{y}?access_token=pk.eyJ1IjoidG9zYW1pc2UiLCJhIjoiY2l6bmxxcjNiMDJtcjJxbzhheW1vemp6bCJ9.aZVIuaxMXKFop4CMStehUQ', {
                  maxZoom: 24,
              }).addTo(mymap);
              var marker = L.marker([lat, long]).addTo(mymap);

              //Get action click to map
              function onMapClick(e) {
                var coor = e.latlng.toString();
                var coor_val = coor.substring( coor.indexOf("(")+1 , coor.indexOf(")") );
                lat = coor_val.substring(0, coor_val.indexOf(","));
                long = coor_val.substring(coor_val.indexOf(",")+1);
                console.log(coor_val);
                $("#edit_coordinates").val( coor_val );
                //Check if existed markers, remove it.
                if (marker) {
                  mymap.removeLayer(marker); // remove
                }
                //Add new marker to map on each click
                marker = L.marker([lat, long]).addTo(mymap);
              }
              mymap.on('click', onMapClick);
            },
          }
        });
      }
    },
    error: function(){
      swal("Đã xảy ra lỗi, hãy thử lại");
      $.magnificPopup.close();
    }
  });//end ajax
});

//Action when click to edit button and otherwise
$("#edit_btn").on("click", function()
{
  if( $("#show_form").is(":visible") )
  {
    $("#edit_form").show(200);
    $("#show_form, .close_btn").hide(200);
    $("#save_btn").show();
    $("#edit_btn").html("<i class='fa fa-undo'></i> Quay về");
  }
  else
  {
    $("#edit_form, #save_btn").hide(200);
    $("#show_form, .close_btn").show(200);
    $("#edit_btn").html("<i class='fa fa-pencil'></i> Chỉnh sửa");
  }
});

$("#save_btn").on("click", function()
{
  $("#edit_form").submit();
});

$("#add_btn").on("click", function()
{
  $("#add_form").submit();
});


/***************
****************
* Validation
****************
***************/

$("#edit_form").validate({
  rules: {
    sup_name: {
      required: true,
    },
    sup_phone: {
      required: true,
      minlength: 10,
      maxlength: 12,
      validPhone: true,
    },
    sup_website: {
      required: false,
      validURL: true,
    }
    
  },
  messages: {
    sup_name: "Ô này cần được nhập thông tin",
    sup_phone: {
      required: "Ô này cần được nhập thông tin",
      minlength: "Điện thoại có ít nhất 10 số",
      maxlength: "Số điện thoại cần ít hơn 12 số",
      validPhone: "Số điện thoại chỉ là chữ số thôi",
    },
    sup_website: "Đường dẫn bị sai rồi, copy luôn http/https luôn nha",
  },
  highlight: function(element) {
      $(element).parents('.input-wrap').addClass('has-error');
      $('#save_btn').prop('disabled',true);
  },
  unhighlight: function(element) {
      $(element).parents('.input-wrap').removeClass('has-error');
      $('#save_btn').prop('disabled',false);
  },
  errorElement: 'span',
  errorClass: 'help-block error display-inbl ml20 mr20',
  errorPlacement: function(error, element) {
    error.insertAfter(element);
  },
  submitHandler: function (form) {
    var data = {
      'id' : $('#edit_id').val(),
      'map' : $('#edit_coordinates').val(),
      'sup_name' : $("#edit_sup_name").val(),
      'sup_phone' : $("#edit_sup_phone").val(),
      'sup_address' : $("#edit_sup_address").val(),
      'sup_website' : $("#edit_sup_website").val(),
      'bank_info' : $("#edit_bankinfo").val(),
    };
    swal({
      title: "Bạn chắc chưa",
      text: "Thông tin sẽ được cập nhật",
      type: "warning",
      showCancelButton: true,
      confirmButtonColor: "#008d4c",
      confirmButtonText: "Ừ làm đi",
      cancelButtonText: "Không, đợi tý",
      closeOnConfirm: true
    }, function(isConfirm){
      if(isConfirm)
      {
        if( $("#edit_form").valid() )
        {
          $.ajax({
            type: "POST",
            url: "<?php echo $this->Url->build( [ 'action' => 'ajax' ] ) ?>",
            data: {update_data: data},
            success: function(data){
              location.reload();
            },
            complete: function(){
              $('#divLoading').removeClass('show');
            },
            error: function(){
              $('#divLoading').removeClass('show');
              swal("Đã xảy ra lỗi, hãy thử lại");
            }
          });//end ajax
        }
      }
    });//is_confirm
  }
});

$("#add_form").validate({
  rules: {
    sup_name: {
      required: true,
    },
    sup_phone: {
      required: true,
      minlength: 10,
      maxlength: 12,
      validPhone: true,
    },
    sup_website: {
      required: false,
      validURL: true,
    }
    
  },
  messages: {
    sup_name: "Ô này cần được nhập thông tin",
    sup_phone: {
      required: "Ô này cần được nhập thông tin",
      minlength: "Điện thoại có ít nhất 10 số",
      maxlength: "Số điện thoại cần ít hơn 12 số",
      validPhone: "Số điện thoại chỉ là chữ số thôi",
    },
    sup_website: "Đường dẫn bị sai rồi, copy luôn http/https luôn nha",
  },
  highlight: function(element) {
      $(element).parents('.input-wrap').addClass('has-error');
      $('#save_btn').prop('disabled',true);
  },
  unhighlight: function(element) {
      $(element).parents('.input-wrap').removeClass('has-error');
      $('#save_btn').prop('disabled',false);
  },
  errorElement: 'span',
  errorClass: 'help-block error display-inbl ml20 mr20',
  errorPlacement: function(error, element) {
    error.insertAfter(element);
  },
  submitHandler: function (form) {
    var data = {
      'map' : $('#add_coordinates').val(),
      'sup_name' : $("#add_sup_name").val(),
      'sup_phone' : $("#add_sup_phone").val(),
      'sup_address' : $("#add_sup_address").val(),
      'sup_website' : $("#add_sup_website").val(),
      'bank_info' : $("#add_bankinfo").val(),
    };
    swal({
      title: "Bạn chắc chưa",
      text: "Thông tin sẽ được cập nhật",
      type: "warning",
      showCancelButton: true,
      confirmButtonColor: "#008d4c",
      confirmButtonText: "Ừ làm đi",
      cancelButtonText: "Không, đợi tý",
      closeOnConfirm: true
    }, function(isConfirm){
      if(isConfirm)
      {
        console.log(data);
        $.ajax({
          type: "POST",
          url: "<?php echo $this->Url->build( [ 'action' => 'ajax' ] ) ?>",
          data: {add_supplier: data},
          success: function(data){
            location.reload();
          },
          complete: function(){
            $('#divLoading').removeClass('show');
          },
          error: function(){
            $('#divLoading').removeClass('show');
            swal("Đã xảy ra lỗi, hãy thử lại");
          }
        });//end ajax
      }
    });//is_confirm
  }
});

});//end ready

/* Debug data */
// $("#add_sup_name").val( "Chợ váy cưới" );
// $("#add_sup_phone").val( "0909456789" );
// $("#add_sup_address").val( "01 Lê Duẩn" );
// $("#add_sup_website").val( "https://facebook.com" );
// $("#add_bankinfo").val( "Ngân hàng Đông Á" );

</script>
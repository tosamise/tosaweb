<?php 
	$url = "/upload/pro_images/";
	$path = IMG_UPLOAD . DS . "pro_images" . DS;
?>

<?= $this->Html->css('jqzoom.css'); ?>
<?= $this->Html->css('justifiedGallery.min.css'); ?>
<?= $this->Flash->render('flash') ?>
<div class="row">
	<div class="form-group">
	<form id="add_img_to_pro" action="/photos" method="post">
    <?= $this->Form->input('pro_id', [
                                'empty' => ['0'=>'Hãy chọn mã sản phẩm'],
                                'options' => $procodes,
                                'class'=>'form-control w300 display-in',
                                'id' => 'pro_id',
                                'error'=>false,
                                'label' => false,
                                'templates' => [
		                              'inputContainer' => '<div class="display-inbl">{{content}}</div>'
			                          ],
                            ]) ?>
   	<button class="btn btn-app save_btn" id="add_img"><i class="fa fa-save"></i> Thêm hình vào sản phẩm</button>
		<?= $this->Form->hidden('img_id', ['value'=> '','id'=>'img_id'] );?>
		<?= $this->Form->hidden('cur_pro_img_num', ['value'=> '','id'=>'cur_pro_img_num'] );?>
  <form>
  </div>
</div>
<?= $this->Form->hidden('select_delete', ['value'=> '','id'=>'select_delete'] );?>
<label>Hình ảnh của sản phẩm hiện tại</label>
<div class="row mb20" id="img_preview"></div>

<a href="javascript:void(0)" class="btn btn-app display-n" id="del_img_from_pro"><i class="fa fa-trash"></i> Xóa hình khỏi sản phẩm</a>

<hr>
<label>Bạn đã chọn các ảnh:</label> <span id="select-result">none</span>
<div class="row">
	<div id="mygallery" >
	<?php 
		foreach($photos as $photo):
			$image_path = $url.$photo->img_name;
			//Physiscally check image existed
			if( !file_exists( $path.$photo->img_name ) )
			{
				$image_path = "/img/no-image.png";
			}
	?>
		<a href="javascript: void(0)" class="ui-widget-content">
			<img class="pi" src="<?= $image_path ?>" title="<?= $photo->img_name ?>" id="<?= $photo->id ?>" width="auto" />
		</a>
	<?php endforeach; ?>
	</div>

	<div class="box-footer text-center"> 
	  <ul class="pagination pagination-sm no-margin pull-right">
	    <?php 
	      // Shows the next links
	      echo $this->Paginator->prev('« Previous');
	      //Shows the page numbers
	      echo $this->Paginator->numbers();
	      // Shows the previous links
	      echo $this->Paginator->next('Next »');
	    ?>
	  </ul>
	</div>
</div>

<?= $this->Html->script('jquery.justifiedGallery.min.js'); ?>
<script type="text/javascript">

  if( localStorage['pro_id'] )
  {
    $("#pro_id").val( localStorage['pro_id'] );
    select_proID();
  }

	$("#mygallery").justifiedGallery({
		rowHeight : 150,
    lastRow : 'nojustify',
    margins : 3
	});

	//Handler to get the array of selected images
  $(function() {
    $("#mygallery").bind("mousedown", function(e) {
        e.metaKey = true;
    }).selectable({
    	//filter: "img.pi:lt(4)",
    	selecting: function(event, ui) {
    		var id_range = $("#img_id").val().split(",");
    		var cur_range = $("#cur_pro_img_num").val();
    		var avail_range = 5 - cur_range;
    		//console.log(avail_range);
    		if( id_range.length >= avail_range ) //range begin with 0
    		{
          // If yes, just remove it from the current selection
          $(ui.selecting).removeClass('ui-selecting');
          swal("Chỉ chọn tối đa "+avail_range+" hình thôi nhé");
        }
			},
      stop: function() {
        var result = $( "#select-result" ).empty();
        $("#img_id").val("");
        var selectedID = [];
        $( ".ui-selected img", this ).each(function() {
        	//Show selected images' name to view	
          result.append( $(this).attr("title")+", " );
          //Set selected images' id into hidden field.
          selectedID.push( $(this).attr("id") );
          $("#img_id").val( selectedID );
        });
      }
    });
  });

  $("#add_img_to_pro").validate({
  	ignore: "", //Also check hidden input
    rules: {
      img_id: {
        required: true,
      },
      pro_id: {
        checkSelect: true,
      },
    },
    messages: {
      img_id: {
        required: "Xin hãy chọn 05 hình sản phẩm",
      },
      pro_id: {
        checkSelect: "Xin hãy chọn mã sản phẩm",
      },
    },
    errorElement: 'span',
    errorClass: 'help-block error',
    errorPlacement: function(error, element) {
        if( element.attr('id') == "img_id" ){
        	error.insertAfter(element);
        } 
        else {
          error.insertAfter(element.parent().next());
        }
    },
    submitHandler: function(form){
    	swal({
        title: "Bạn chắc chắn chưa?",
        text: "Các ảnh sẽ được thêm vào sản phẩm",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#008d4c",
        confirmButtonText: "Thêm vào",
        cancelButtonText: "Không, đợi tý",
        closeOnConfirm: true
      }, function(isConfirm){
        if(isConfirm)
        {
        	form.submit();
        }
      });
    },
  });

  $("#pro_id").on("change",function(){
    if( ! $("#del_img_from_pro").hasClass("display-n") )
    {
      $("#del_img_from_pro").addClass("display-n");
    }
    select_proID();
  });

  function select_proID() {
    //Set session this selection
    if( $("#pro_id").val() != 0 ){
      localStorage['pro_id'] = $("#pro_id").val();
    }else{
      localStorage['pro_id'] = "";
    }
  	$.ajax({
      type: "POST",
      url: "<?php echo $this->Url->build( [ 'action' => 'ajax' ] ) ?>",
      data: {get_pro_imgs: $("#pro_id").val()},
      //async: false,
      success: function(data){
        if(data != "")
    		{
    			//Parse data got from server as JSON
    			var obj = jQuery.parseJSON(data);
    			var data = obj['data'];
    			var html_img = "";
          var imglinks = "<?= $url ?>";
          var cur_pro_img_num = 0;
          if( data != "")
          {
          	// console.log(data);
            for( var i = 0; i < data.length; i++ )
            {
              html_img += "<img src='"+imglinks+data[i]['img_name']+"' title='"+data[i]['img_name']+"' class='img_preview w130 mr5 ui-widget-content minh200' id='"+data[i]['id']+"' >";
              cur_pro_img_num++;
            }
              
          }else{
  	    		html_img = "<i>Hiện sản phẩm này không có hình ảnh nào</i>";
          }
          //Set current number of product's photo existing
          $("#cur_pro_img_num").val(cur_pro_img_num);
          //Set html to view for preview current product photos
          $("#img_preview").html(html_img);
          //Unselect all items when product code change
          $("#mygallery").children().removeClass("ui-selected");
          //Unset all data of photo ids
          $("#img_id").val("");
          //Remove div loading
          $('#divLoading').removeClass('show');

          $(function() {
  			    $("#img_preview").bind("mousedown", function(e) {
  			        e.metaKey = true;
  			    }).selectable({
  			    	stop: function() {
  			        $("#select_delete").val("");
  			        var selectedID = [];
  			        $( ".ui-selected", this ).each(function() {
  			          //Set selected images' id into hidden field.
  			          selectedID.push( $(this).attr("id") );
  			          $("#select_delete").val( selectedID );
  			        });
  			      },
  			      selected: function (event, ui) {
  			      	$("#del_img_from_pro").removeClass("display-n");
  						},
  			      unselected: function (event, ui) {
  			      	var a = $("#select_delete").val().split(",");
  			      	if( a.length <= 1 )
  			      	{
  			        	$("#del_img_from_pro").addClass("display-n");
  			        }
  				    }
  			    });
  			  });
    		}//end if
      },
      complete: function(){
        $('#divLoading').removeClass('show');
      },
      error: function(){
        $('#divLoading').removeClass('show');
        swal("Đã xảy ra lỗi, hãy thử lại");
      }
    });//end ajax
  }

  $("#del_img_from_pro").on("click",function(){
  	swal({
      title: "Bạn chắc chắn chưa?",
      text: "Các ảnh sẽ bị xóa",
      type: "warning",
      showCancelButton: true,
      confirmButtonColor: "#008d4c",
      confirmButtonText: "Xoá luôn",
      cancelButtonText: "Không, đợi tý",
      closeOnConfirm: true
    }, function(isConfirm){
      if(isConfirm)
      {
      	$.ajax({
          type: "POST",
          url: "<?php echo $this->Url->build( [ 'action' => 'ajax' ] ) ?>",
          data: { del_img_from_pro: $("#select_delete").val() },
          async: false,
          success: function(data){
            var Obj = jQuery.parseJSON(data);
            if(Obj.data == true)
            {
              location.reload();
            }
          },
          complete: function(){
            $('#divLoading').removeClass('show');
          },
          error: function(){
            $('#divLoading').removeClass('show');
            swal("Đã xảy ra lỗi, hãy thử lại");
          }
        });//end ajax
      }
    });
  });

</script>
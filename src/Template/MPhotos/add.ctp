<?php 
  $url = "/upload/pro_images/";
  $path = IMG_UPLOAD . DS . "pro_images" . DS;
?>
<?php echo $this->Html->css('dropzone.css'); ?>

<div class="row">
  <div class="col-md-12" id="wrap-dropzone">
    <!-- general form elements -->
    <div class="box box-warning">
      <!-- /.box-header -->
      <div class="box-body">
        
        <?= $this->Form->create(null, ['enctype' => 'multipart/form-data', 'class'=>'dropzone drop-style','id'=>'my-awesome-dropzone']) ?>
        
           <?php $this->Form->file('file_name', ['class'=>'form-control','id'=>'file_name','multiple'=>true,'div'=>false,'label'=>'','error'=>false] ); ?>
            
            <?= $this->Form->hidden('created', ['value'=> date('Y-m-d H:i:s')] );?>

        <?= $this->Form->end() ?>
        
        <?= $this->Form->button(__('<i class="fa fa-upload"></i> Tải lên'), ['id'=>'upload-image','class'=>'btn btn-app fr mt10','escape'=>false]); ?>
      </div>
      <!-- /.box-body -->
    </div>
    <!-- /.box -->
  </div> 
</div>
<!-- /.row -->

<?php echo $this->Html->script('dropzone.js'); ?>
<script type="text/javascript">
Dropzone.autoDiscover = false;
Dropzone.options.myAwesomeDropzone = {
  accept: function(file, done) {
    done();
  },
  init: function() {
    this.on("addedfile", function() {
    if (this.files[10]!=null){
        swal("Error", "Mỗi lần tải 10 hình thôi nhé", "error");
        this.removeFile(this.files[0]);
        }
    });
  }
};

var errors = "";
var myDropzone = new Dropzone("#my-awesome-dropzone", {
  url: "<?php echo $this->Url->build( [ 'action' => 'add' ] ) ?>",
  dictDefaultMessage: "Kéo thả ảnh vào đây",                      
  autoProcessQueue: false,
  acceptedFiles: "image/*",
  addRemoveLinks: true,
  parallelUploads: 10,
  maxFilesize: 5, // MB
  removedfile: function(file) {
    var _ref;
    return (_ref = file.previewElement) != null ? _ref.parentNode.removeChild(file.previewElement) : void 0;
  },
  error: function(file, errorMessage) {
    errors = true;
  },
  queuecomplete: function() {
    //console.log(errors);
    if( (errors != null) && errors) swal("Lỗi", "Tải lên thất bại", "error");
    else swal("Xong !", "Các ảnh của bạn đã được tải lên", "success");
    Dropzone.forElement("#my-awesome-dropzone").removeAllFiles(true);
    //location.reload();
  }
});

$('#upload-image').click(function(){
  swal({
    title: "Bạn đã chắc?",
    text: "Các hình bạn chọn sẽ được tải lên server",
    type: "warning",
    showCancelButton: true,
    confirmButtonColor: "#F39814",
    confirmButtonText: "Tải lên",
    cancelButtonText: "Không, đợi tý",
    closeOnConfirm: true
  }, function() {
      myDropzone.processQueue();
  });
});
</script>
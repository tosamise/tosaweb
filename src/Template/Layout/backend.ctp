<?php
  use Cake\Core\Configure;
  $pageInfo = Configure::read('info');
  $envirSetting = Configure::read('Enviroment');
  //Use for highlight the active content
  $controller = $this->request->params['controller'];
  $action = $this->request->params['action'];
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <?php echo $this->Html->charset(); ?>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <?php $envir; if( $envirSetting && $envirSetting == 1 ){ $envir = "(Dev) "; } ?>
    <title><?= $envir.$title ?></title>

    <!-- Bootstrap core CSS -->
    <?php echo $this->Html->css('bootstrap.min.css'); ?>
    <!-- Fontawesome 4 -->
    <?php echo $this->Html->css('font-awesome.min.css'); ?>
    <!-- Jquery UI CSS -->
    <?= $this->Html->css('//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css'); ?>
    <!-- Plugin sweetalert -->
    <?= $this->Html->css('sweetalert.css'); ?>
    <!-- Custom styles for this template -->
    <?php echo $this->Html->css('common.css'); ?>
    <!-- Custom styles for this template -->
    <?php echo $this->Html->css('dashboard.css'); ?>

    <!-- Main jquery -->
    <?php echo $this->Html->script('jquery.min.js'); ?>
    <!-- Jquery validation -->
    <?php echo $this->Html->script('jquery.validate.min.js'); ?>
    <?= $this->Html->script('customJQueryValidationMethod.js'); ?>
    <!-- Jquery UI JS -->
    <?= $this->Html->script('https://code.jquery.com/ui/1.12.1/jquery-ui.js'); ?>
    <!-- Plugin Sweetalert -->
    <?= $this->Html->script('sweetalert.min.js'); ?>

  </head>

  <body>

    <nav class="navbar navbar-inverse navbar-fixed-top">
      <div class="container-fluid">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="/"><?= $pageInfo['project_name'] ?></a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
          <ul class="nav navbar-nav navbar-right">
            <li><a href="#">Dashboard</a></li>
            <li><a href="#">Settings</a></li>
            <li><a href="#">Profile</a></li>
            <li><a href="#">Help</a></li>
          </ul>
          <form class="navbar-form navbar-right">
            <input type="text" class="form-control" placeholder="Search...">
          </form>
        </div>
      </div>
    </nav>

    <div class="container-fluid">
      <div class="row">
        
        <?= $this->element('left_menu') ?>

        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
          
          <div id="divLoading" class=""></div>

          <?= $this->fetch('content') ?>

        </div>
      </div>
    </div>

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <?php echo $this->Html->script('bootstrap.min.js'); ?>
    <?= $this->fetch('script') ?>
    <script type="text/javascript">
      $(document).ready(function()
      {
        var controller = "<?= $controller ?>";
        var action = "<?= $action ?>";

        switch(""+controller+"|"+action+"")
        {
          case "MProducts|index":
            $("#products_menu").addClass("active");    
            break;
          case "Infos|category":
          case "Infos|supplier":
            $("#info_menu").addClass("active");
            break;
          case "MPhotos|index":
            $("#photo_menu").addClass("active");
            break;
          case "Files|import":
          case "Files|export":
            $("#file_menu").addClass("active");
            break;
          case "Orders|add":
          case "Orders|index":
            $("#order_menu").addClass("active");
            break;
        }

      });
    </script>

  </body>
</html>

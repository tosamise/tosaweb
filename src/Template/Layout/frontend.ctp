<?php
  use Cake\Core\Configure;
  $pageInfo = Configure::read('info');
  $envirSetting = Configure::read('Enviroment');
  //Use for highlight the active content
  $controller = $this->request->params['controller'];
  $action = $this->request->params['action'];
?>
<?php 
  $baseimgURL = $this->Link->getBaseImgUrl();
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">

    <?php $envir; if( $envirSetting && $envirSetting == 1 ){ $envir = "(Dev) "; } ?>
    <title><?= $envir.$title ?></title>

    <!-- Bootstrap core CSS -->
    <?php echo $this->Html->css('bootstrap.min.css'); ?>
    <!-- Fontawesome 4 -->
    <?php echo $this->Html->css('font-awesome.min.css'); ?>

    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <!-- <link href="../../assets/css/ie10-viewport-bug-workaround.css" rel="stylesheet"> -->

    <!-- Custom styles for this template -->
    <?php echo $this->Html->css('common.css'); ?>
    <!-- Custom styles for this template -->
    <?php echo $this->Html->css('justified-nav.css'); ?>
    <!-- Custom styles for this template -->
    <?php echo $this->Html->css('frontend.css'); ?>

    <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
    <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
    <!-- <script src="../../assets/js/ie-emulation-modes-warning.js"></script> -->

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!-- Main jquery -->
    <?php echo $this->Html->script('jquery.min.js'); ?>


  </head>

  <body>

    <div class="container">

      <!-- The justified navigation menu is meant for single line per list item.
           Multiple lines will require custom code not provided by Bootstrap. -->
      <div class="masthead">
        <h3 class="text-muted display-inbl">
          <?= $this->Html->image("logo.png",['class'=>'w100']); ?>
          <?= $pageInfo['project_name'] ?>
        </h3>
        <h5 class="text-muted display-inbl fr text-r">
          <p>0909.566.926 - 0967.696.974</p>
          <p>71 Tầng 2 Ký Con, P.Nguyễn Thái Bình, Quận 1, Tp.HCM</p>
          <p>Mở cửa: 18:30 - 21:00 (T2-T6) | 08:00 đến 21:00 (T7-CN)</p>
          <p>Xin vui lòng gọi điện trước</p>
        </h5>
        <nav>
          <ul class="nav nav-justified">
            <li class="<?= ($controller == "Pages" && $action == "home"?"active":"") ?>">
              <a href="#">Trang chủ</a>
            </li>
            <li class="<?= ($controller == "Pages" && $action == "showroom"?"active":"") ?>">
              <a href="#">Sản phẩm</a>
            </li>
            <li><a href="#">Dịch vụ</a></li>
            <li><a href="#">Thư viện</a></li>
            <li><a href="#">Về chúng tôi</a></li>
            <li><a href="#">Liên lạc</a></li>
          </ul>
        </nav>
      </div>

      <!-- Jumbotron -->
     <!--  <div class="jumbotron">
        <h1>Marketing stuff!</h1>
        <p class="lead">Cras justo odio, dapibus ac facilisis in, egestas eget quam. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet.</p>
        <p><a class="btn btn-lg btn-success" href="#" role="button">Get started today</a></p>
      </div> -->
      <div id="banner-wrap">
        <?= $this->Html->image("banner.jpg",['id'=>'banner']); ?>
      </div>

      <!-- Example row of columns -->
      <!-- <div class="row">
        <div class="col-lg-4">
          <h2>Safari bug warning!</h2>
          <p class="text-danger">As of v9.1.2, Safari exhibits a bug in which resizing your browser horizontally causes rendering errors in the justified nav that are cleared upon refreshing.</p>
          <p>Donec id elit non mi porta gravida at eget metus. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus. Etiam porta sem malesuada magna mollis euismod. Donec sed odio dui. </p>
          <p><a class="btn btn-primary" href="#" role="button">View details &raquo;</a></p>
        </div>
        <div class="col-lg-4">
          <h2>Heading</h2>
          <p>Donec id elit non mi porta gravida at eget metus. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus. Etiam porta sem malesuada magna mollis euismod. Donec sed odio dui. </p>
          <p><a class="btn btn-primary" href="#" role="button">View details &raquo;</a></p>
       </div>
        <div class="col-lg-4">
          <h2>Heading</h2>
          <p>Donec sed odio dui. Cras justo odio, dapibus ac facilisis in, egestas eget quam. Vestibulum id ligula porta felis euismod semper. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa.</p>
          <p><a class="btn btn-primary" href="#" role="button">View details &raquo;</a></p>
        </div>
      </div> -->

      <div class="row">
        <div class="col-xs-12 col-sm-9 col-md-12">

          <div id="divLoading" class="" style="background-image : url('<?= $baseimgURL?>loading.gif');"></div>

          <?= $this->fetch('content') ?>

        </div><!--/.col-xs-12.col-sm-9-->  
      </div><!--/row-->
      
      <!-- Site footer -->
      <footer class="footer">
        <p>&copy; 2017 Tosa L'amour. All right reserved</p>
      </footer>

    </div> <!-- /container -->


    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <!-- <script src="../../assets/js/ie10-viewport-bug-workaround.js"></script> -->

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <?php echo $this->Html->script('bootstrap.min.js'); ?>
    <?= $this->fetch('script') ?>

  </body>
</html>
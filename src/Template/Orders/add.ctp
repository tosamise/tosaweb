<?php 
  $url = "/upload/pro_images/";
  $path = IMG_UPLOAD . DS . "pro_images" . DS;
?>

<form id="add_form">
  <h3>Thông tin khách hàng</h3>
  <div class="form-group">
    <label for="cus_name">Tên khách hàng</label>
    <input type="text" class="form-control" id="cus_name" name="cus_name">
  </div>
  <div class="form-group">
    <label for="cus_address">Địa chỉ</label>
    <input type="text" class="form-control" id="cus_address" name="cus_address">
  </div>
  <div class="form-group">
    <label for="cus_email">Email</label>
    <input type="email" class="form-control" id="cus_email" name="cus_email">
  </div>
  <div class="form-group">
    <label for="cus_note">Ghi chú</label>
    <textarea id="cus_note" class="form-control" rows="5" name="cus_note"></textarea>
  </div>
  
  <h3>Thông tin đặt hàng</h3>
  <!-- Date select -->
  <div class="">
    <div class="form-group">
      <label for="rent_date">Ngày lấy</label>
      <input type="text" class="form-control" id="rent_date" name="rent_date">
    </div>
    <div class="form-group">
      <label for="back_date">Ngày trả</label>
      <input type="text" class="form-control" id="back_date" name="back_date">
    </div>
  </div>
  <!-- Product select -->
  <div class="">
    <div class="form-group">
      <h4>Sản phẩm</h4>
      <input type="button" class="btn btn-success" id="add_pro" value="Thêm sản phẩm">
      <div class="mt20" id="pro_img_list">
        <div class="pro_item display-in">
          <img src="/upload/pro_images/A0001.jpg" class="w150" />
        </div>
        <div class="pro_item display-in">
          <img src="/upload/pro_images/A0002.jpg" class="w150" />
        </div>
        <div class="pro_item display-in">
          <img src="/upload/pro_images/A0003.jpg" class="w150" />
        </div>
      </div>
    
      
      <?php // $this->Form->input('pro_id', [
                            //     'empty' => ['0'=>'Hãy chọn mã sản phẩm'],
                            //     'options' => $procodes,
                            //     'class'=>'form-control',
                            //     'id' => 'pro_id',
                            //     'error'=>false,
                            //     'label' => false,
                            //     'templates' => [
                            //       'inputContainer' => '{{content}}'
                            //     ],
                            // ]) ?>
    </div>
    
  </div>

<br><br><br>
  <button type="submit" class="btn btn-default">Lưu</button>
</form>
<?= $this->Html->script('order.js'); ?>




<br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>
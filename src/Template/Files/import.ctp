<?= $this->Flash->render('flash') ?>
<style type="text/css">
	.cake-debug-output{margin-left: 250px!important;}
</style>
<form method="post" id="import_form" action="/import" enctype="multipart/form-data">
	<div class="form-group">
		<label for="import_file">Nhập file</label>
		<input type="file" id="import_file" name="file">
		<p class="help-block">Chọn file để nhập vào hệ thống</p>
	</div>
	<input class="btn btn-app" type="submit" id="submit_btn" value="Tải lên">
</form>

<div class="mt50">
	<i>Từ Google Spreadsheet. Chọn Tệp -> Tải xuống định dạng -> Microsoft Excel (xlsx)</i>
	<img src="/img/tip_import_excel.jpg" class="w800">
</div>
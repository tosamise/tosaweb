<div class="col-sm-3 col-md-2 sidebar">
  <ul class="main-menu nav nav-sidebar">
    <li><a href="#">Tổng quan <span class="sr-only">(current)</span></a></li>
    <li><a href="#">Báo cáo</a></li>
    <li><a href="#">Phân tích</a></li>
    <li id="order_menu" class="">
      <a id="branch_order" href="javascript:void(0)" data-toggle="collapse" data-target="#submenu_order" aria-expanded="false">Đơn hàng <i class="fa fa-angle-down fr"></i></a>
      <ul id="submenu_order" class="nav collapse ml20" role="menu" aria-labelledby="branch_order">
        <li>
          <a href="/add-order/">Thêm đơn hàng</a>
        </li>
        <li>
          <a href="/order/">Xem, tìm kiếm</a>
        </li>
      </ul>
    </li>
  </ul>
  <ul class="nav nav-sidebar">
    <li id="products_menu" class="">
      <a href="/products">Sản phẩm</a></li>
    <li id="info_menu" class="">
      <a id="branch_info" href="javascript:void(0)" data-toggle="collapse" data-target="#submenu_info" aria-expanded="false">Thông tin <i class="fa fa-angle-down fr"></i></a>
      <ul id="submenu_info" class="nav collapse ml20" role="menu" aria-labelledby="branch_info">
        <li>
          <a href="/category/">Danh mục</a>
        </li>
        <li>
          <a href="/supplier/">Nhà cung cấp</a>
        </li>
      </ul>
    </li>
    <li id="photo_menu" class="">
      <a id="branch_photo" href="javascript:void(0)" data-toggle="collapse" data-target="#submenu_photo" aria-expanded="false">Hình ảnh <i class="fa fa-angle-down fr"></i></a>
      <ul id="submenu_photo" class="nav collapse ml20" role="menu" aria-labelledby="branch_photo">
        <li>
          <a href="/photos">Thư viện ảnh</a></li>
        </li>
        <li>
          <a href="/add-photo">Thêm ảnh</a></li>
        </li>
      </ul>
    <li id="file_menu" class="">
      <a id="branch_file" href="javascript:void(0)" data-toggle="collapse" data-target="#submenu_file" aria-expanded="false">Xuất, nhập file <i class="fa fa-angle-down fr"></i></a>
      <ul id="submenu_file" class="nav collapse ml20" role="menu" aria-labelledby="branch_file">
        <li>
          <a href="/import/">Nhập file</a>
        </li>
        <li>
          <a href="/export/">Xuất file</a>
        </li>
      </ul>
    </li>
  </ul>
</div>
<?= $this->Html->css('jquery.dataTables.min.css'); ?>
<?= $this->Html->css('magnific-popup.css'); ?>
<?= $this->Html->css('jqzoom.css'); ?>
<?= $this->Flash->render('flash') ?>
<?= $this->Form->select('category',$cates,['class'=>'form-control mb20 minw200 maxw300',
                                           'id'=>'change_cate',
                                           'empty'=>["Chọn danh mục"],
                                           'value'=>(!empty($chosen_cate)?$chosen_cate:'0')
]); ?>
<?= $this->Form->input('search',['class'=>'form-control mb20 w300',
                                 'id'=>'search_box',
                                 'label'=>false,
                                 'placeholder'=>'Nhập để tìm kiếm',
                                 'templates' => [
                                    'inputContainer' => '<div class="display-inbl">{{content}}</div>'
                                  ],
                                ]); ?>

<a href='#add_popup' class='btn btn-default pb40' id="open_add_popup"><i class="fa fa-plus"></i> Thêm sản phẩm</a>

<table id="product_table" class="table table-hover stripe">
  <thead>
  <tr>
    <th>STT</th>
    <th>Mã sản phẩm</th>
    <th>Tình trạng %</th>
    <th>Giá xưởng</th>
    <th>Giá cơ bản</th>
    <th>Giá thuê</th>
    <th>Giá bán</th>
    <th>Ngày nhập</th>
  </tr>
  </thead>
  <tbody>
  <?php $no = "1" ?>
  <?php foreach($products as $product): ?>
  <tr>
    <td><?= $no ?></td>
    <td class="qk-pcode"><?= $product->cate_code.$product->pro_code ?></td>
    <td class="quality editable"><?= ($product->quality?$product->quality:"-") ?></td>
    <td class="price factory_price editable"><?= ($product->price?$product->price:"-") ?></td>
    <td class="price origin_price editable"><?= ($product->price_up?$product->price_up:"-") ?></td>
    <td class="price rent_price"></td>
    <td class="price sell_price"></td>
    <td class="editable imported_date"><?= ($product->import_date?$product->import_date:"-") ?></td>
    <?php $no++ ?>
  </tr>
  <?php endforeach; ?>
  </tbody>
</table>

<!-- Popup edit -->
<div id="view_detail_popup" class="custom_popup w80p mfp-hide white-popup-block" style="border: 3px solid #f39c12;">
  <div class="box-body">
      <!-- <button class='close_btn btn btn-app fr'><i class="fa fa-times"></i> Đóng</button> -->

      <div class="row">
        <div class="col-md-5 minh150 minw150 text-c">
            <div class="bzoom_wrap row ml40" id="preview_images">
              <ul id="bzoom">
                <!-- Photo will be added in here by javascript -->
              </ul>
            </div>
        </div>
        <div class="col-md-7">
          <div class="row">
              <div class="col-md-12" style="font-size:50px">
                <b>#<span id="preview_pro_code"></span></b>
                <i id="edit_btn" class="edit-btn fa fa-pencil" aria-hidden="true" title="Chỉnh sửa"></i>
                <i class="red-text fsize20" id="sold_mark"></i>
              </div>
          </div>
          <div class="row">
            <!-- Show information popup -->
            <div id="show_form" class="box box-default mb20" style="font-size:18px">
              <div class="col-md-4 mt20"><b>Nhà cung cấp:</b></div>
              <div class="col-md-8 mt20"><span id="preview_supplier">&nbsp</span></div>
              <div class="col-md-4 mt20"><b>Màu sắc:</b></div>
              <div class="col-md-8 mt20"><span id="preview_color">&nbsp</span></div>
              <div class="col-md-4 mt20"><b>Tình trạng:</b></div>
              <div class="col-md-5 mt20"><span id="preview_quality">&nbsp</span></div>
              <div class="col-md-4 mt20"><b>Số lượng:</b></div>
              <div class="col-md-5 mt20"><span id="preview_quantity">&nbsp</span></div>
              <div class="col-md-4 mt20"><b>Giá xưởng:</b></div>
              <div class="col-md-5 mt20 price-font"><span id="preview_factory_price">&nbsp</span></div>
              <div class="col-md-4 mt20"><b>Giá cơ bản:</b></div>
              <div class="col-md-5 mt20 price-font"><span id="preview_origin_price">&nbsp</span></div>
              <div class="col-md-4 mt20"><b>Giá thuê:</b></div>
              <div class="col-md-5 mt20 price-font"><span id="preview_rent_price">&nbsp</span></div>
              <div class="col-md-4 mt20"><b>Giá bán:</b></div>
              <div class="col-md-5 mt20 price-font"><span id="preview_sell_price">&nbsp</span></div>
              <div class="col-md-4 mt20"><b>Ngày nhập:</b></div>
              <div class="col-md-5 mt20"><span class="tags" id="preview_created">&nbsp</span></div>
            </div>
            <!-- Show edit form popup -->
            <form id="edit_form" class="edit-form" method="post" action="/edit-product">
              <div id="show_form" class="box box-default mb20" style="font-size:18px">
              <div class="col-md-4 mt10">
              <?= $this->Form->input('cate_code', [
                            'options' => $cates,
                            'id' => 'edit_cate_code',
                            'class' => 'form-control',
                            'error' => false,
                            'label' => false,
                            'placeholder' => 'Chọn danh mục',
                            'templates' => [
                                'inputContainer' => '<div class="display-inbl">{{content}}</div>'
                            ],
                        ]) ?>
              </div>
              <div class="col-md-5 mt10">
                <input class="form-control display-inbl" id="edit_pro_code" name="pro_code">
              </div>
              <div class="col-md-4 mt10"><b>Nhà cung cấp:</b></div>
              <div class="col-md-5 mt10">
                <?= $this->Form->input('sup_id', [
                                      'options' => $suppliers,
                                      'id' => 'edit_supplier',
                                      'class'=>'form-control',
                                      'error'=>false,
                                      'label' => false,
                                  ]) ?>
              </div>
              <div class="col-md-4 mt10"><b>Màu sắc:</b></div>
              <div class="col-md-5 mt10"><input class="form-control" id="edit_color" name="color"></div>
              <div class="col-md-4 mt10"><b>Tình trạng:</b></div>
              <?php 
                  $quality = [];
                  for($i = 100; $i>=80; $i = $i-5 )
                  {
                      $quality += [$i => $i];
                  } 
              ?>
              <div class="col-md-5 mt10">
                <?= $this->Form->input('quality_stt', [
                    'options' => $quality,
                    'id' => 'edit_quality',
                    'class'=>'form-control',
                    'error'=>false,
                    'label' => false,
                    'div' => false
                ]) ?>
              </div>
              <div class="col-md-4 mt10"><b>Số lượng:</b></div>
              <div class="col-md-5 mt10">
                <input class="form-control" id="edit_quantity" name="quantity">
              </div>
              <div class="col-md-4 mt10"><b>Giá xưởng:</b></div>
              <div class="col-md-5 mt10"><input class="form-control price-font" id="edit_price" name="price"></div>
              <div class="col-md-4 mt10"><b>Giá cơ bản:</b></div>
              <div class="col-md-5 mt10"><input class="form-control price-font" id="edit_price_up" name="price_up"></div>
              <div class="col-md-4 mt10"><b>Giá thuê:</b></div>
              <div class="col-md-5 mt10"><input class="form-control price-font" id="edit_rent_price" name="rent_price" readonly></div>
              <div class="col-md-4 mt10"><b>Giá bán:</b></div>
              <div class="col-md-5 mt10"><input class="form-control price-font" id="edit_sell_price" name="sell_price" readonly></div>
              <div class="col-md-4 mt10"><b>Ngày nhập:</b></div>
              <div class="col-md-5 mt10 mb50"><input class="form-control" id="edit_import_date" name="import_date"></div>
            </div>
            <input type="hidden" value="" id="pro_id" class="form-control" />
            <input type="hidden" value="" id="cur_pro_code" class="form-control" />
            <input type="hidden" value="<?= date('Y-m-d h:i:s'); ?>" id="modified" />
            </form>
          </div>
        </div>  
      </div>

      <button class='close_btn btn btn-app fr pb40'><i class="fa fa-times"></i> Đóng</button>
      <button class='btn btn-app fr pb40 mr20' id="back_btn"><i class="fa fa-undo"></i> Quay lại</button>
      <button class='btn btn-app fr pb40 mr20' id="save_btn"><i class="fa fa-save"></i> Lưu</button>
      <button class='btn btn-app fr pb40 mr20' id="sold_btn"><i class="fa fa-shopping-cart"></i> Đã bán</button>
  </div>
  <!-- /.box-body -->
</div>

<!-- Popup add product -->
<div id="add_popup" class="custom_popup w50p mfp-hide white-popup-block" style="border: 3px solid #f39c12;">
  <div class="box-body">
    <div class="row">
      <div class="col-md-12">
        <div class="row">
            <div class="col-md-12" style="font-size:50px">
              <b>#<span id="preview_new_pro_code">...</span></b></i>
            </div>
        </div>
        <div class="row">
          <form id="add_form" class="add-form" method="post" action="/add-product">
            <div id="show_form" class="box box-default mb20" style="font-size:18px">
            <div class="col-md-12 mt10">
            <?= $this->Form->input('cate_code', [
                          'options' => $cates,
                          'empty' => ['0'=>'Chọn danh mục'],
                          'id' => 'new_cate_code',
                          'class' => 'form-control w200',
                          'error' => false,
                          'label' => false,
                          'placeholder' => 'Chọn danh mục',
                          'templates' => [
                              'inputContainer' => '<div class="display-inbl">{{content}}</div>'
                          ],
                      ]) ?>
            <div class="error-area display-inbl"></div>
            </div>
            <div class="col-md-12 mt10">
              <?= $this->Form->input('pro_code',[
                             'id'=>'new_pro_code',
                             'class'=>'form-control display-inbl w200',
                             'placeholder'=>'Số sản phẩm',
                             'title'=>'Số sản phẩm',
                             'error' => false,
                             'label' => false,
                             'templates' => [
                              'inputContainer' => '<div class="display-inbl">{{content}}</div>'
                              ],
                            ]); ?>
              <div class="error-area display-inbl"></div>
              <span style="font-size: 14px;">Mới nhất: </span><span id="last_id">xxxx</span>
            </div>
            <div class="col-md-12 mt10">
              <?= $this->Form->input('sup_id', [
                            'options' => $suppliers,
                            'empty' => ['0'=>'Chọn nhà cung cấp'],
                            'id' => 'new_supplier',
                            'class'=>'form-control w200',
                            'error'=>false,
                            'label' => false,
                            'templates' => [
                                'inputContainer' => '<div class="display-inbl">{{content}}</div>'
                            ],
                        ]) ?>
              <div class="error-area display-inbl"></div>
            </div>
            <div class="col-md-12 mt10">
              <?= $this->Form->input('color',['id'=>'new_color',
                                              'class'=>'form-control w200',
                                              'placeholder'=>'Màu sắc',
                                              'error'=>false,
                                              'label' => false,
                                              'templates' => [
                                                'inputContainer'=>'<div class="display-inbl">{{content}}</div>'],
                                              ]); ?>
              <div class="error-area display-inbl"></div>
            </div>
            <?php 
                $quality = [];
                for($i = 100; $i>=80; $i = $i-5 )
                {
                    $quality += [$i => $i];
                } 
            ?>
            <div class="col-md-12 mt10">
              <?= $this->Form->input('quality_stt', [
                              'options' => $quality,
                              'empty' => ['0'=>"Tình trạng"],
                              'id' => 'new_quality',
                              'class'=>'form-control w200',
                              'error'=>false,
                              'label' => false,
                              'templates' => [
                                'inputContainer' => '<div class="display-inbl">{{content}}</div>'
                              ],
                          ]) ?>
              <div class="error-area display-inbl"></div>
              <i>%</i>
            </div>
            <div class="col-md-12 mt10">
              <?= $this->Form->input('quantity',[
                             'id'=>'new_quantity',
                             'class'=>'form-control w200',
                             'placeholder'=>'Số lượng',
                             'error'=>false,
                             'label' => false,
                             'default'=>'1',
                             'templates' => [
                              'inputContainer'=>'<div class="display-inbl">{{content}}</div>'],
                            ]); ?>
              <div class="error-area display-inbl"></div>
            </div>
            <div class="col-md-12 mt10">
              <?= $this->Form->input('price',[
                             'id'=>'new_price',
                             'class'=>'form-control w200',
                             'placeholder'=>'Giá xưởng',
                             'error'=>false,
                             'label' => false,
                             'templates' => [
                              'inputContainer'=>'<div class="display-inbl">{{content}}</div>'],
                            ]); ?>
              <div class="error-area display-inbl"></div>
            </div>
            <div class="col-md-12 mt10">
              <?= $this->Form->input('price_up',[
                             'id'=>'new_price_up',
                             'class'=>'form-control w200',
                             'placeholder'=>'Giá cơ bản',
                             'error'=>false,
                             'label' => false,
                             'templates' => [
                              'inputContainer'=>'<div class="display-inbl">{{content}}</div>'],
                            ]); ?>
              <div class="error-area display-inbl"></div>
              <?= $this->Form->input('rent_price',['id'=>'new_rent_price',
                             'class'=>'form-control w200',
                             'placeholder'=>'Giá thuê',
                             'title'=>'Giá thuê',
                             'readonly'=>true,
                             'error'=>false,
                             'label' => false,
                             'templates' => [
                              'inputContainer'=>'<div class="display-inbl">{{content}}</div>'],
                            ]); ?>
                        
              <?= $this->Form->input('sell_price',[
                             'id'=>'new_sell_price',
                             'class'=>'form-control w200',
                             'placeholder'=>'Giá bán',
                             'title'=>'Giá bán',
                             'readonly'=>true,
                             'error'=>false,
                             'label' => false,
                             'templates' => [
                              'inputContainer'=>'<div class="display-inbl">{{content}}</div>'],
                            ]); ?>
              <div class="error-area display-inbl"></div>
            </div>
            <div class="col-md-12 mt10 mb50">
              <?= $this->Form->input('import_date',[
                             'id'=>'new_import_date',
                             'class'=>'form-control w200',
                             'placeholder'=>'Ngày nhập',
                             'title'=>'Ngày nhập',
                             'error'=>false,
                             'label' => false,
                             'templates' => [
                              'inputContainer'=>'<div class="display-inbl">{{content}}</div>'],
                            ]); ?>
            </div>
          </div>
          <input type="hidden" value="<?= date('Y-m-d h:i:s'); ?>" id="creted" />
          </form>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-md-12" style="margin-bottom: -70px;">
        <button class='close_btn btn btn-app fr pb40'><i class="fa fa-times"></i> Đóng</button>
        <button class='btn btn-app fr pb40 mr20' id="submit_btn"><i class="fa fa-save"></i> Lưu</button>
        <a href='#add_cate_popup' class='btn btn-default pb40' id="add_cate_btn" title="Thêm danh mục"><i class="fa fa-plus"></i> Thêm danh mục mới</a>
        <a href='#add_sup_popup' class='btn btn-default pb40' id="add_sup_btn" title="Thêm nhà cung cấp"><i class="fa fa-plus"></i> Thêm nhà cung cấp mới</a>
      </div>
    </div>
  </div>
  <!-- /.box-body -->
</div>

<!-- Popup add category -->
<div id="add_cate_popup" class="custom_popup w50p mfp-hide white-popup-block" style="border: 3px solid #f39c12;">
  <div class="box-body">
    <div class="row">
      <div class="col-md-12">
        <div class="form-group">
        <?= $this->Form->create(null, ['id'=>'add_cate_form']) ?>
          <div class="input-group">
            <div class="input-group-addon">
                <i class="fa fa-code"></i>
            </div>
            <?= $this->Form->input('cate_code', ['class'=>'form-control','id'=>'cate_code','placeholder'=>'Mã danh mục','div'=>false,'label'=>'','error'=>false] ); ?>
          </div>
          <?= $this->Form->error('cate_code', null, array('class' => 'error-message','div'=>false)); ?>

          <div class="input-group mt10">
            <div class="input-group-addon">
                <i class="fa fa-font"></i>
            </div>
            <?= $this->Form->input('cate_name', ['class'=>'form-control','id'=>'cate_name','placeholder'=>'Tên danh mục','div'=>false,'label'=>'','error'=>false] ); ?>
          </div>
          <?= $this->Form->error('cate_name', null, array('class' => 'error-message','div'=>false)); ?>
          <?= $this->Form->end() ?>
        </div>
        <!-- end form-group -->
      </div>
    </div>
    <div class="row">
      <div class="col-md-12" style="margin-bottom: -70px;">
        <button class='back_btn btn btn-app fr pb40'><i class="fa fa-undo"></i> Quay lại</button>
        <button class='save_btn btn btn-app fr pb40 mr20' id="submit_cate_btn"><i class="fa fa-save"></i> Lưu</button>
      </div>
    </div>
  </div>
</div>

<!-- Popup add supplier -->
<div id="add_sup_popup" class="custom_popup w50p mfp-hide white-popup-block" style="border: 3px solid #f39c12;">
  <div class="box-body">
    <div class="row">
      <div class="col-md-12">
      <div class="form-group">
      <?= $this->Form->create(null, ['url' => 'add-supplier','id'=>'add_sup_form']) ?>

        <div class="input-group mt10">
          <div class="input-group-addon">
              <i class="fa fa-font"></i>
          </div>
          <?= $this->Form->input('sup_name', ['class'=>'form-control','id'=>'sup_name','placeholder'=> 'Tên hiển thị','div'=>false,'label'=>'','error'=>false] ); ?>
        </div>

        <div class="input-group mt10">
          <div class="input-group-addon">
              <i class="fa fa-phone"></i>
          </div>
          <?= $this->Form->input('sup_phone', ['class'=>'form-control','id'=>'sup_phone','placeholder'=>'Điện thoại','div'=>false,'label'=>'','error'=>false] ); ?>
        </div>

        <div class="input-group mt10">
          <div class="input-group-addon">
              <i class="fa fa-map"></i>
          </div>
          <?= $this->Form->input('sup_address', ['class'=>'form-control','id'=>'sup_address','placeholder'=>'Địa chỉ','div'=>false,'label'=>'','error'=>false] ); ?>
        </div>

        <div class="input-group mt10">
          <div class="input-group-addon">
              <i class="fa fa-link"></i>
          </div>
          <?= $this->Form->input('sup_website', ['class'=>'form-control','id'=>'sup_website','placeholder'=>'Trang chủ','div'=>false,'label'=>'','error'=>false] ); ?>
        </div>

        <div class="input-group mt10">
          <div class="input-group-addon">
              <i class="fa fa-university"></i>
          </div>
          <?= $this->Form->input('bank_info', ['class'=>'form-control','id'=>'sup','placeholder'=>'Thông tin ngân hàng','div'=>false,'label'=>'','error'=>false,'type'=>'textarea'] ); ?>
        </div>

      <?= $this->Form->end() ?>
      </div>
      </div>
    </div>
    <div class="row">
      <div class="col-md-12" style="margin-bottom: -70px;">
        <button class='back_btn btn btn-app fr pb40'><i class="fa fa-undo"></i> Quay lại</button>
        <button class='btn btn-app save_btn fr pb40 mr20' id="submit_sup_btn"><i class="fa fa-save"></i> Lưu</button>
      </div>
    </div>
  </div>
</div>

<?= $this->Html->script('jquery.dataTables.min.js'); ?>
<?= $this->Html->script('jquery.magnific-popup.min.js'); ?>
<?= $this->Html->script('jquery.number.js'); ?>
<?= $this->Html->script('price.js'); ?>
<?= $this->Html->script('jqzoom.js'); ?>

<script type="text/javascript">
$(document).ready(function(){

  //Create datapicker for input date
  $( function() {
    $( "#edit_import_date, #new_import_date" ).datepicker({
      dateFormat: "dd/mm/yy",
      changeMonth: true,
      changeYear: true,
      yearRange: ((new Date).getFullYear()-5) + ':' + ((new Date).getFullYear()),
      maxDate: new Date
    });
  });

  //Handle the price with origin price
  $(".origin_price").each(function(){
    var ObjPrice = new Price();
    ObjPrice.setInfo( Number($(this).text()) , Number($(this).prev().prev(".quality").text()) );
    $(this).next('.rent_price').text(""+ObjPrice.calRentPrice()+"đ");
    var a = ObjPrice.calSellPrice(); 
    $(this).next().next('.sell_price').text(""+a+"đ");
  });

  //Create the datatable
  var oTable  = $('#product_table').DataTable({
      "oLanguage": {
        "oPaginate": {
          "sFirst":    "Trang đầu",
          "sLast":    "Trang cuối",
          "sNext":    "Trang kế",
          "sPrevious": "Trang trước"
        },
        "sInfo": "Hiển thị _START_ đến _END_ trên tổng _TOTAL_ records",
        "sInfoEmpty": "Hiển thị 0 dữ liệu",
        "sZeroRecords": "Không tìm thấy dữ liệu",
      },
      "bLengthChange": false, //Hide select box
      "columnDefs": [
        { className: "dt-right", "targets": [3,4,5,6] },
        { className: "dt-center", "targets": [0,1,2,7] },
        {
          targets: 1,
          render: function(row, type, val, meta) {
            return '<a href="#view_detail_popup" class="view_detail" id="'+val[1]+'">'+val[1]+'</a>';
          }
        },
        {
          targets: -1,
          render: function(row, type, val, meta) {
            var date = new Date( val[7] );
            var day = date.getDate();
            if(day<10){ day = "0"+day; }
            var month = (date.getMonth()+1);
            if(month<10){ month = "0"+month; }
            return day+"/"+month+"/"+date.getFullYear();
          }
        },
        {
          targets: [3,4,5,6],
          render: function(data) {
            return $.number( data , 0 , ',' );
          }
        },
      ]
  });

  //Action when search is input.
  $("#search_box").keyup(function(){
    oTable.search($(this).val()).draw() ;
  })

  //Format currency
  //$('.price').number( true , 0 , ',' );

  //Action change category change the data of page.
  $("#change_cate").on('change',function(){
    var cate_code = $(this).val();
    window.location.replace("/products?cate="+cate_code+"");
  })

  //Initialize the magnific popup
  $(function () {
    $('.view_detail').magnificPopup({
      //type: 'inline',
      preloader: false,
      focus: '#view_detail_popup',
      closeBtnInside:true,
      modal: true
    });
    $(document).on('click', '.close_btn', function (e) {
      e.preventDefault();
      $.magnificPopup.close();
      $('#edit_form').hide();
      $('#show_form').show();
      $('#edit_btn').removeClass("fa-ban");
      $('#edit_btn').addClass("fa-pencil");
      $('#edit_btn').attr('title','Chỉnh sửa');
    });
  });

  //Action when click to view detail
  $(".view_detail").on('click',function(){
    $.ajax({
      type: 'POST',
      url: '<?php echo $this->Url->build( [ 'action' => 'ajax' ] ) ?>',
      data: { 'view_detail': $(this).attr('id') },
      async: false,
      dataType: 'json',
      success: function(result){
        //console.log(result);
        var Obj = result.data['pro_detail'][0];
        var ObjImg = result.data['pro_imgs'];
        if(Obj != "")
        {
          //Empty all fields before input new data
          $("#preview_pro_code").text("");
          $("#preview_supplier").text("");
          $("#preview_color").text("");
          $("#preview_quality").text("");
          $("#preview_quantity").text("");
          $("#preview_factory_price").text("");
          $("#preview_origin_price").text("");
          $("#preview_rent_price").text("");
          $("#preview_sell_price").text("");
          $("#preview_created").text("");
          $('#edit_form').hide();
          $('#save_btn').hide();
          $('#sold_btn').hide();
          $('#back_btn').hide();

          //Empty all edit input before input data
          $("#edit_pro_code").val("");
          $("#edit_color").val("");
          $("#edit_quantity").val("");
          $("#edit_price").val("");
          $("#edit_price_up").val("");
          $("#cur_pro_code").val("");
          $("#pro_id").val("");
          $("#sold_mark").text("");

          //Set data into fields
          //Set review images
          var url = "/upload/pro_images/";
          var img_class_thumb = "bzoom_thumb_image pro_img popup-img col-md-10 col-xs-12";
          var img_class_big = "bzoom_big_image pro_img popup-img col-md-10 col-xs-12";
          var pre_html = ""; 
          if( ObjImg.length > 0 )
          {
            //ONLY 4 thumbnails are OK
            for (var i = 0; i < 4; i++)
            {
              //If product have images
              if( ObjImg[i] ){
                var img_name = ObjImg[i].img_name;
                var img_url = url+img_name;
                pre_html += "<li> \
                             <img src='"+img_url+"' class='"+img_class_thumb+"' alt='"+img_name+"'> \
                             <img src='"+img_url+"' class='"+img_class_big+"' alt='"+img_name+"'> \
                             </li>";
              }
              //If don't have images, use default
              else
              {
                pre_html += "<li> \
                             <img src='/img/no-image.png' class='"+img_class_thumb+"' alt='no-image.png'> \
                             <img src='/img/no-image.png' class='"+img_class_big+"' alt='no-image.png'> \
                             </li>";
              }
            }
            $("#bzoom").html(pre_html);
            createSlide(); //Call function to create slide
          }
          
          //Product detail
          $("#preview_pro_code").html( Obj.cate_code + Obj.pro_code );
          $("#preview_supplier").html("<a href='"+Obj.supplier_web+"' target='_blank' >"+Obj.supplier+"</a>");
          $("#preview_color").html( (Obj.color?Obj.color:"-") );
          $("#preview_quality").html( (Obj.quality?Obj.quality:"100")+"%" );
          $("#preview_quantity").html( (Obj.quantity?Obj.quantity:"01") );
          //Handle price
          $("#preview_factory_price").html( $.number( Obj.price, 0, ',' )+"đ" );
          $("#preview_origin_price").html( $.number( Obj.price_up, 0, ',' )+"đ" );
          var ObjPrice = new Price();
          ObjPrice.setInfo(Obj.price_up,Obj.quality);
          $("#preview_rent_price").html($.number( ObjPrice.calRentPrice(), 0, ',' )+"đ");
          $("#preview_sell_price").html($.number( ObjPrice.calSellPrice(), 0, ',' )+"đ");
          //Handle date format
          var date = new Date( Obj.import_date );
          var day = date.getDate();
          if(day<10){ day = "0"+day; }
          var month = date.getMonth()+1;
          if (month < 10){ month = "0"+month }
          $("#preview_created").html( day+"/"+month+"/"+date.getFullYear() );
          //Selling status
          if( Obj.is_sold == 1 )
          {
            $("#sold_mark").html("Đã bán");
            $("#sold_btn").hide();
          }
          else
          {
            $("#sold_btn").show();
          }
          
          //Add data to edit form
          $("#edit_pro_code").val( Obj.cate_code );
          $("#edit_pro_code").val( Obj.pro_code );
          $("#edit_supplier").val( Obj.sup_id );
          $("#edit_color").val( (Obj.color?Obj.color:"") );
          $("#edit_quality").val( Obj.quality?Obj.quality:"100" );
          $("#edit_quantity").val( Obj.quantity?Obj.quantity:"01" );
          $("#edit_price").val( $.number( Obj.price, 0, ',' ) );
          $("#edit_price_up").val( $.number( Obj.price_up, 0, ',' ) );
          $("#edit_rent_price").val( $.number( ObjPrice.calRentPrice(), 0, ',' ) );
          $("#edit_sell_price").val( $.number( ObjPrice.calSellPrice(), 0, ',' ) );
          $("#edit_import_date").datepicker('setDate', date);
          $("#cur_pro_code").val(Obj.cate_code + Obj.pro_code);//Add product code to hidden to compare when change
          $("#pro_id").val( Obj.id );//set product id to save the editing data
        }
      },
      error: function(){
        swal("Đã xảy ra lỗi, hãy thử lại");
        $.magnificPopup.close();
      }
    });//end ajax
  })

  //Action when click to edit button and otherwise
  $("#edit_btn, #back_btn").on("click", function()
  {
    if( $("#show_form").is(":visible") )
    {
        $('#edit_btn').removeClass("fa-pencil");
        $('#edit_btn').addClass("fa-ban");
        $('#edit_btn').attr('title','Hủy');
        $('#show_form').hide(200);
        $('#edit_form').show(200);
        $('#save_btn,#back_btn').show();
        $('.close_btn ').hide();
    }
    else
    {
        $('#edit_btn').removeClass("fa-ban");
        $('#edit_btn').addClass("fa-pencil");
        $('#edit_btn').attr('title','Chỉnh sửa');
        $('#show_form').show(200);
        $('#edit_form').hide(200);
        $('#save_btn, #back_btn').hide();
        $('.close_btn ').show();
    }
  });

  $('#save_btn').on('click',function(e){
    var d = new Date($("#edit_import_date").val().split("/").reverse().join("-"));
    var newDate = d.getFullYear()+'-'+(d.getMonth()+1)+'-'+d.getDate();
    //console.log(date);
    e.preventDefault();
    var data = {
      'id' : $("#pro_id").val(),
      'price' : $("#edit_price").val().replace(/[^\d\.]/g, ''),
      'price_up' : $("#edit_price_up").val().replace(/[^\d\.]/g, ''),
      'pro_code' : $("#edit_pro_code").val(),
      'cate_code' : $("#edit_cate_code").val(),
      'sup_id' : $("#edit_supplier").val(),
      'quality_stt' : $("#edit_quality").val(),
      'quantity' : $("#edit_quantity").val(),
      'color' : $("#edit_color").val(),
      'import_date' : newDate,
      'modified' : $("#modified").val(),
    };
    swal({
      title: "Bạn chắc chưa",
      text: "Thông tin sẽ được cập nhật",
      type: "warning",
      showCancelButton: true,
      confirmButtonColor: "#008d4c",
      confirmButtonText: "Ừ làm đi",
      cancelButtonText: "Không, đợi tý",
      closeOnConfirm: true
    }, function(isConfirm){
      if(isConfirm)
      {
        $.ajax({
          type: "POST",
          url: "<?php echo $this->Url->build( [ 'action' => 'ajax' ] ) ?>",
          data: {update_data: data},
          success: function(data){
            location.reload();
          },
          complete: function(){
            $('#divLoading').removeClass('show');
          },
          error: function(){
            $('#divLoading').removeClass('show');
            swal("Đã xảy ra lỗi, hãy thử lại");
          }
        });//end ajax
      }
    });
  });

  $("#sold_btn").on("click", function(e)
  {
    e.preventDefault();
    swal({
      title: "Bạn có chắc đã bán rồi",
      text: "Bạn không thể quay lại đâu nhé",
      type: "warning",
      showCancelButton: true,
      confirmButtonColor: "#008d4c",
      confirmButtonText: "Ừ bán rồi",
      cancelButtonText: "Ý chưa, hủy nha",
      closeOnConfirm: true
    }, function(isConfirm){
      if(isConfirm)
      {
        $.ajax({
          type: "POST",
          url: "<?php echo $this->Url->build( [ 'action' => 'ajax' ] ) ?>",
          data: {sold: $('#pro_id').val()},
          success: function(data){
              location.reload();
          },
          complete: function(){
              $('#divLoading').removeClass('show');
          },
          error: function(){
              $('#divLoading').removeClass('show');
              swal("Đã xảy ra lỗi, hãy thử lại");
          }
        });//end ajax
      }
    });
  });

  /*
  * Create quick edit data in row
  */
  var cur_id = "";
  var cur_data = "";
  $("#product_table").on("dblclick", ".editable" , function(e){
    //Find the quick input to check only 1 is existed
    if( $("#product_table").find("input").length < 1 && $("#product_table").find("select").length < 1)
    {
      cur_id = $(this).prevAll("td.qk-pcode").text(); //Get this line product Code.
      cur_data = $(this).text();
      var input = "";
      //Identify field is edited to set field for update database.
      var cur_field = "";
      if( $(this).hasClass('quality') )
      {
        input = "<select class='w150' id='qk_input' db-field='quality_stt'> \
                 <option value='80'>80</option> \
                 <option value='85'>85</option> \
                 <option value='90'>90</option> \
                 <option value='95'>95</option> \
                 <option value='100'>100</option> \
                 </select>";
      }
      else if( $(this).hasClass('factory_price') )
      {
        cur_data = cur_data.replace(/[^\d\.]/g,'')
        input = "<input class='w150 text-r' id='qk_input' value='"+cur_data+"' db-field='price' />";
      }
      else if( $(this).hasClass('origin_price') )
      {
        cur_data = cur_data.replace(/[^\d\.]/g,'')
        input = "<input class='w150 text-r' id='qk_input' value='"+cur_data+"' db-field='price_up' />";
      }
      else if( $(this).hasClass('imported_date') )
      {
        input = "<input class='w150 text-r' id='qk_input' value='"+cur_data+"' db-field='import_date' />";
      }
     
      var html_content = input+"<i class='fa fa-check success ml5 cur-point' id='qk_ok' title='Lưu nhanh'></i> \
                   <i class='fa fa-ban error ml5 cur-point' id='qk_cancel' title='Hủy'></i>";
      
      //Execute html content to view
      $(this).html(html_content);

      //if input is selectbox, set default data for it
      if( $(this).hasClass('quality') )
      {
        $("#qk_input").val(cur_data);
      }
      //if input is select date
      if( $(this).hasClass('imported_date') )
      {
        $("#qk_input").datepicker({
          dateFormat: "dd/mm/yy",
          changeMonth: true,
          changeYear: true,
          yearRange: ((new Date).getFullYear()-5) + ':' + ((new Date).getFullYear()),
          maxDate: new Date
        });
      }
      //if input is currency
      if( $(this).hasClass('origin_price') )
      {
        $("#qk_input").val(cur_data);
        $("#qk_input").on('change keyup',function(e){
          var ObjPrice = new Price();
          ObjPrice.setInfo( $(this).val() , Number( $("#qk_input").parent().prev().prev().text() ) );
          $(this).parent().next().text($.number( ObjPrice.calRentPrice(), 0, ',' ));
          $(this).parent().next().next().text($.number( ObjPrice.calSellPrice(), 0, ',' )); 
        });
      }
    }
  });

  //Click to close quick editor
  $('.editable').on('click', '#qk_cancel', function() {
    $(this).prevAll('input').remove();
    if(/^\d{2}\/\d{2}\/\d{4}$/.test(cur_data))
    {
      $(this).parent().text(cur_data);
    }
    else
    {
      $(this).parent().text($.number( cur_data , 0, ',' ));
    }
  });

  //Click to close quick editor and save data
  $('.editable').on('click', '#qk_ok', function() {
    var edited_field = $(this).prevAll('#qk_input').attr('db-field');
    var edited_data = $(this).prevAll('#qk_input').val();
    //Check the data is date
    if(/^\d{2}\/\d{2}\/\d{4}$/.test(edited_data))
    {
      var d = new Date(edited_data.split("/").reverse().join("-"));
      var old_date = edited_data;
      var edited_data = d.getFullYear()+'-'+(d.getMonth()+1)+'-'+d.getDate();
      $(this).prevAll('#qk_input').remove();
      $(this).parent().text(old_date);
    }
    else
    {
      var currency = $.number( edited_data , 0, ',' );
      $(this).prevAll('#qk_input').remove();
      $(this).parent().text(currency);
    }

    //Call ajax to save this data
    $.ajax({
      type: "POST",
      url: "<?php echo $this->Url->build( [ 'action' => 'ajax' ] ) ?>",
      data: {quick_save: edited_field, pro_code: cur_id, edited_data: edited_data },
      success: function(data){
        obj = jQuery.parseJSON(data);
        if(obj['data'] == false){
          swal("Đã xảy ra lỗi, hãy thử lại");
        }
      },
      complete: function(){
        $('#divLoading').removeClass('show');
      },
      error: function(){
        swal("Đã xảy ra lỗi, hãy thử lại");
        $('#divLoading').removeClass('show');
      }
    });//end ajax
  });

  /*
  * ADD Product
  */

  //Initialize the magnific popup
  $(function () {
    $('#open_add_popup').magnificPopup({
      type: 'inline',
      preloader: false,
      closeBtnInside:true,
      focus: '#add_popup',
      modal: true
    });
    $(document).on('click', '.back_btn', function (e) {
      e.preventDefault();
      $.magnificPopup.open({
        items: {
          src: '#add_popup',
          callbacks: {
            beforeOpen: function () {
               
            }
          }
        }
      });//end magnificPopup
    });
  });

  //Initialize the magnific popup add category
  $(function () {
    $('#add_cate_btn').magnificPopup({
      type: 'inline',
      preloader: false,
      focus: '#add_cate_popup',
      closeBtnInside:true,
      modal: true
    });
    $(document).on('click', '.back_btn', function (e) {
      e.preventDefault();
      $.magnificPopup.open({
        items: {
          src: '#add_popup',
        }
      });//end magnificPopup
    });
  });//end function

  //Initialize the magnific popup add supplier
  $(function () {
    $('#add_sup_btn').magnificPopup({
      type: 'inline',
      preloader: false,
      focus: '#add_sup_popup',
      closeBtnInside:true,
      modal: true
    });
    $(document).on('click', '.close_btn', function (e) {
      e.preventDefault();
      $.magnificPopup.close();
    });
  });

  //Find the newest product id when change category
  $("#new_cate_code").on("change", function()
  {
    $.ajax({
      type: "POST",
      url: "<?php echo $this->Url->build( [ 'action' => 'ajax' ] ) ?>",
      data: {find_newest: $(this).val() },
      success: function(data){
        obj = jQuery.parseJSON(data);
        if(obj["data"])
        {
          $("#last_id").text(obj["data"]["pro_code"]); 
        }
        else
        {
          $("#last_id").text("Chưa có");
        }
      },
      complete: function(){
        $('#divLoading').removeClass('show');
      },
      error: function(){
        swal("Đã xảy ra lỗi, hãy thử lại");
        $('#divLoading').removeClass('show');
      }
    });//end ajax
  });

  //Get&Set data to preview pro_code when typing
  $("#new_pro_code, #new_cate_code").on("keyup change", function()
  {
    $("#preview_new_pro_code").text(""+$("#new_cate_code").val()+$("#new_pro_code").val() );
  });

  //Handle when input new price
  //Format currency factory price
  $("#new_price").on("keyup change", function(){
    $(this).val( $.number( $(this).val() , 0, ',' ) );  
  });

  //Format and calculate origin price to rent & sell price
  $("#new_price_up").on("keyup change", function(){
    //Auto format currency type
    $(this).val( $.number( $(this).val() , 0, ',' ) );
    //Auto calculate and set rent & sell price to input
    if( $("#new_quality").val() != 0 )
    {
      calculatePrice( $(this).val().replace(/[^\d\.]/g, '') , $("#new_quality").val() , $("#new_rent_price"), $("#new_sell_price") );
    }
    else
    {
      swal("Hãy chọn tình trạng trước");
    }
  });

  //Calculate price when change quality
  $("#new_quality").on("change", function(){
    if( $("#new_price_up").val() != "" )
    {
      calculatePrice( $("#new_price_up").val().replace(/[^\d\.]/g, '') , $(this).val() , $("#new_rent_price"), $("#new_sell_price") );
    }
  });

  //Handle submit button when click for add form
  $("#submit_btn").on("click",function(){
    swal({
      title: "Bạn chắc chắn chưa?",
      text: "Sản phẩm mới sẽ được thêm vào hệ thống",
      type: "warning",
      showCancelButton: true,
      confirmButtonColor: "#008d4c",
      confirmButtonText: "Thêm vào",
      cancelButtonText: "Không, đợi tý",
      closeOnConfirm: true
    }, function(isConfirm){
      if(isConfirm)
      {
        $("#add_form").submit();
      }
    });
  });

  //ADD CATEGORY POPUP
  $("#submit_cate_btn").on('click',function(){
    //Get data
    var data = {
      'cate_code' : $("#cate_code").val(),
      'cate_name' : $("#cate_name").val(),
    };
    if( $("#add_cate_form").valid() )
    {
      swal({
        title: "Bạn chắc chắn chưa?",
        text: "Danh mục mới sẽ được thêm vào hệ thống",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#008d4c",
        confirmButtonText: "Thêm vào",
        cancelButtonText: "Không, đợi tý",
        closeOnConfirm: true
      }, function(isConfirm){
        if(isConfirm)
        {
          $.ajax({
            type: "POST",
            url: "<?php echo $this->Url->build( [ 'action' => 'ajax' ] ) ?>",
            data: {qk_add_cate: data},
            //async: false,
            success: function(data){
              //location.reload();
              var Obj = jQuery.parseJSON(data);
              if(Obj != "")
              {
                //Add new option to dropdownbox
                $("#new_cate_code").append("<option value='"+Obj.data.cate_code+"'>"+Obj.data.cate_name+"</option>");

                //After add new option to select, open add popup after 2s
                setTimeout(function(){
                  swal("Thêm thành công");
                  $.magnificPopup.open({
                    items: {
                      src: '#add_popup',
                    }
                  });
                }, 2000);
              }
            },
            complete: function(){
              $('#divLoading').removeClass('show');
            },
            error: function(){
              $('#divLoading').removeClass('show');
              swal("Đã xảy ra lỗi, hãy thử lại");
            }
          });//end ajax
        }
      });
    }
  });

  //Auto capitalize word when typing
  $("#cate_code").on('keyup change',function(){
    $(this).val( $(this).val().toUpperCase() );
  })

  //ADD SUPPLIER POPUP
  $("#submit_sup_btn").on('click',function(){
    //Get data
    var data = {
      'sup_name' : $("#sup_name").val(),
      'sup_phone' : $("#sup_phone").val(),
      'sup_address' : $("#sup_address").val(),
      'sup_website' : $("#sup_website").val(),
      'bank_info' : $("#bank_info").val(),
    };

    if( $("#add_sup_form").valid() )
    {
      swal({
        title: "Bạn chắc chắn chưa?",
        text: "Thông tin nhà cung cấp mới sẽ được thêm vào hệ thống",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#008d4c",
        confirmButtonText: "Thêm vào",
        cancelButtonText: "Không, đợi tý",
        closeOnConfirm: true
      }, function(isConfirm){
        if(isConfirm)
        {
          $.ajax({
            type: "POST",
            url: "<?php echo $this->Url->build( [ 'action' => 'ajax' ] ) ?>",
            data: {qk_add_sup: data},
            //async: false,
            success: function(data){
              //location.reload();
              var Obj = jQuery.parseJSON(data);
              if(Obj != "")
              {
                //Add new option to dropdownbox
                $("#new_supplier").append("<option value='"+Obj.data.id+"'>"+Obj.data.sup_name+"</option>");
                //Set chosen value is last insert
                $("#new_supplier").val(Obj.data.id);

                //After add new option to select, open add popup after 2s
                setTimeout(function(){
                  swal("Thêm thành công");
                  $.magnificPopup.open({
                    items: {
                      src: '#add_popup',
                    }
                  });
                }, 2000);
              }
            },
            complete: function(){
              $('#divLoading').removeClass('show');
            },
            error: function(){
              $('#divLoading').removeClass('show');
              swal("Đã xảy ra lỗi, hãy thử lại");
            }
          });//end ajax
        }
      });
    }
  });


  /***************
  ****************
  * Validation
  ****************
  ***************/

  /*
  * validation for edit form
  */

  //Get&Set data to preview pro_code when typing
  $("#edit_pro_code, #edit_cate_code").on("keyup change", function()
  {
    $("#preview_pro_code").text(""+$("#edit_cate_code").val()+$("#edit_pro_code").val() );
  });

  //Check product is existed or not.
  $("#edit_pro_code").on("blur", function(){
      $('#save_btn').prop('disabled',true);
      var cate_code = $("#edit_cate_code").val();
      var pro_code = $(this).val();
      var checkcurrent = cate_code+pro_code;
      //Compare with current edit, if same not validate
      if( checkcurrent != $('#cur_pro_code').val() )
      {
        $.ajax({
            type: "POST",
            url: "<?php echo $this->Url->build( [ 'action' => 'ajax' ] ) ?>",
            data: {check_code: pro_code, cate_code: cate_code},
            success: function(data){
              obj = jQuery.parseJSON(data);
              if(!obj["data"])
              {
                  swal("Mã sản phẩm này đã có rồi !!!");
                  $("#edit_pro_code").val("");
              }
              else
              {
                  $('#save_btn').prop('disabled',false);
              }
            },
            complete: function(){
              $('#divLoading').removeClass('show');
            },
            error: function(){
              swal("Đã xảy ra lỗi, hãy thử lại");
              $('#divLoading').removeClass('show');
            }
        });//end ajax
      }
  });

  $("#edit_form").validate({
    rules: {
      quantity: {
        required: true,
        number: true,
        maxlength: 2,
      },
      pro_code: {
        required: true,
        minlength: 4,
        maxlength: 4,
        checkProCode: true
      },
      price: {checkPrice: true,},
      price_up: {checkPrice: true,},
    },
    messages: {
      quantity: {
        required: "Ô này cần được nhập thông tin",
        number: "Chỉ xài số thôi nha",
        maxlength: "Dài nhất 02 số thôi",
      },
      pro_code: {
        required: "Ô này cần được nhập thông tin",
        minlength: "Nên có ít nhất 4 chữ số",
        maxlength: "Dài nhất 4 chữ số thôi",
        checkProCode: "Mã sản phẩm không đúng oy"
      },
      price: {checkPrice: "Ô này cần được nhập thông tin",},
      price_up: {checkPrice: "Ô này cần được nhập thông tin",},
    },
    highlight: function(element) {
        $(element).parents('.input-wrap').addClass('has-error');
        $('#save_btn').prop('disabled',true);
    },
    unhighlight: function(element) {
        $(element).parents('.input-wrap').removeClass('has-error');
        $('#save_btn').prop('disabled',false);
    },
    errorElement: 'span',
    errorClass: 'help-block error',
    errorPlacement: function(error, element) {
        if(element.parents('.input-group').length) {
            error.insertAfter(element.parents(".input-group"));
        } else {
            error.insertAfter(element);
        }
        
    },
  });

  $("#add_form").validate({
    rules: {
      cate_code:{
        checkSelect: true,
      },
      sup_id:{
        checkSelect: true,
      },
      quality_stt:{
        checkSelect: true,
      },
      quantity: {
        required: true,
        number: true,
        maxlength: 2,
      },
      pro_code: {
        required: true,
        minlength: 4,
        maxlength: 4,
        checkProCode: true
      },
      price: {
        checkPrice: true,
        required: true,
      },
      price_up: {
        checkPrice: true,
        required: true,
      },
      import_date: {
        required: true,
        //date: true
      },
    },
    messages: {
      cate_code:{
        checkSelect: "Xin chọn một trong các mục",
      },
      sup_id:{
        checkSelect: "Xin chọn một trong các mục",
      },
      quality_stt:{
        checkSelect: "Xin chọn một trong các mục",
      },
      quantity: {
        required: "Ô này cần được nhập thông tin",
        number: "Chỉ xài số thôi nha",
        maxlength: "Dài nhất {0} số thôi",
      },
      pro_code: {
        required: "Ô này cần được nhập thông tin",
        minlength: "Nên có ít nhất 4 chữ số",
        maxlength: "Dài nhất 4 chữ số thôi",
        checkProCode: "Mã sản phẩm không đúng oy"
      },
      price: {
        checkPrice: "Ô này cần được nhập thông tin",
        required: "Ô này cần được nhập thông tin",
      },
      price_up: {
        checkPrice: "Ô này cần được nhập thông tin",
        required: "Ô này cần được nhập thông tin",
      },
      import_date: {
        required: "Ô này cần được nhập thông tin",
        //date: "Hình như không đúng định dạng ngày rồi",
      },
    },
    highlight: function(element) {
        $(element).parents('.input-wrap').addClass('has-error');
        $('#save_btn').prop('disabled',true);
    },
    unhighlight: function(element) {
        $(element).parents('.input-wrap').removeClass('has-error');
        $('#save_btn').prop('disabled',false);
    },
    errorElement: 'span',
    errorClass: 'help-block error display-inbl ml20 mr20',
    errorPlacement: function(error, element) {
      error.insertAfter(element.parent().next(".error-area"));
    },
  });

  $("#add_cate_form").validate({
    rules: {
      cate_code: {
                  required: true,
                  requiredCaps: true,
                  maxlength: 4,
      },
      cate_name: "required",
    },
    messages: {
      cate_code: {
        required: "Ô này cần được nhập thông tin",
        requiredCaps: "Chỉ sử dụng chữ IN HOA không dấu nhé",
        maxlength: "Mã danh mục dài quá, 4 chữ thôi nhé"
      },
      cate_name: "Ô này cần được nhập thông tin",
    },
    highlight: function(element) {
      $("#submit_cate_btn").attr('disabled',true);
      $(element).closest('.input-group').addClass('has-error');
    },
    unhighlight: function(element) {
      $("#submit_cate_btn").attr('disabled',false);
      $(element).closest('.input-group').removeClass('has-error');
    },
    errorElement: 'span',
    errorClass: 'help-block error',
    errorPlacement: function(error, element) {
        if(element.parents('.input-group').length) {
            error.insertAfter(element.parents(".input-group"));
        } else {
            error.insertAfter(element);
        }
      }
  });  

  $("#add_sup_form").validate({
    rules: {
      sup_name: "required",
      sup_phone: {
        required: true,
        minlength: 10,
        maxlength: 12,
        validPhone: true,
      },
      sup_website: {
        required: false,
        validURL: true,
      }
    },
    messages: {
      sup_name: "Ô này cần được nhập thông tin",
      sup_phone: {
        required: "Ô này cần được nhập thông tin",
        minlength: "Điện thoại có ít nhất 10 số",
        maxlength: "Số ĐT thì ít hơn 12 số",
        validPhone: "Số ĐT thì chỉ là chữ số thôi",
      },
      sup_website: "Đường dẫn bị sai rồi",
    },
    highlight: function(element) {
      $("#submit_sup_btn").attr('disabled',true);
      $(element).closest('.input-group').addClass('has-error');
    },
    unhighlight: function(element) {
      $("#submit_sup_btn").attr('disabled',false);
      $(element).closest('.input-group').removeClass('has-error');
    },
    errorElement: 'span',
    errorClass: 'help-block error',
    errorPlacement: function(error, element) {
        if(element.parents('.input-group').length) {
            error.insertAfter(element.parents(".input-group"));
        } else {
            error.insertAfter(element);
        }
      }
  });

  //Auto format price when typing
  $('#edit_price, #edit_price_up').on('change keyup',function(e){
    $(this).val( $.number( $(this).val() , 0, ',' ) );
  });

  //When click to price input, select all data in
  $('#edit_price, #edit_price_up').on('click',function(e){
    $(this).select();
  });

  //Auto calculate rent and sell price for preview
  $('#edit_price_up, #edit_quality').on('change',function(e){
    calculatePrice( $('#edit_price_up').val().replace(/[^0-9\.]+/g,"") , $("#edit_quality").val() , $("#edit_rent_price") , $("#edit_sell_price"));
  });

  //Check pro_code existed in add category popup
  $("#cate_code").on('change',function(){
    if( $(this).val() != "" )
    {
      $.ajax({
        type: "POST",
        url: "<?php echo $this->Url->build( [ 'action' => 'ajax' ] ) ?>",
        data: {checkCatecode: $(this).val() },
        success: function(data){
          obj = jQuery.parseJSON(data);
          if(!obj["data"])
          {
            swal("Mã sản phẩm này đã có rồi !!!");
            $("#cate_code").val("");
          }
        },
        complete: function(){
          $('#divLoading').removeClass('show');
        },
        error: function(){
          swal("Đã xảy ra lỗi, hãy thử lại");
          $('#divLoading').removeClass('show');
        }
      });//end ajax
    }
  });

  /*
  * validation for add form
  */

  //Check product is existed or not.
  $("#new_pro_code").on("blur", function(){
    $('#submit_btn').prop('disabled',true);
    var cate_code = $("#new_cate_code").val();
    var pro_code = $(this).val();
    //Check cate_code is chosen before input pro_code
    if( cate_code != '0' )
    {
      $.ajax({
        type: "POST",
        url: "<?php echo $this->Url->build( [ 'action' => 'ajax' ] ) ?>",
        data: {check_code: pro_code, cate_code: cate_code},
        success: function(data){
          obj = jQuery.parseJSON(data);
          if(!obj["data"])
          {
            swal("Mã sản phẩm này đã có rồi !!!");
            $("#new_pro_code").val("");
          }
          else
          {

            $('#submit_btn').prop('disabled',false);
          }
        },
        complete: function(){
          $('#divLoading').removeClass('show');
        },
        error: function(){
          swal("Đã xảy ra lỗi, hãy thử lại");
          $('#divLoading').removeClass('show');
        }
      });//end ajax
    }
    else
    {
      swal("Hãy chọn danh mục trước");
      $(this).val("");
    }
  });


  /**
  * Description: Calculate price for rent and selling
  * Function: calculatePrice()
  * @author: HuyDo
  * @params: price_up: origin price, 
  *          quality: product quality,
             rent_element: element for input data
             sell_element: element for input data
  * @return: array data
  */
  function calculatePrice( price_up, quality, rent_element, sell_element )
  {
    var ObjPrice = new Price();
    ObjPrice.setInfo( price_up , quality );
    rent_element.val($.number( ObjPrice.calRentPrice(), 0, ',' ));
    sell_element.val($.number( ObjPrice.calSellPrice(), 0, ',' ));
  }

  //Create photo preview slideshow
  function createSlide(){
    $("#bzoom").zoom({
      zoom_area_width: 300,
      autoplay_interval :3000,
      small_thumbs : 4,
      autoplay : false
    });
  }


});//end ready
</script>

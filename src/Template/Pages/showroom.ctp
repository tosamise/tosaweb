<?php 
  $baseimgURL = $this->Link->getBaseImgUrl();
  $url = $this->Link->getUploadImgUrl();
  $path = $this->Link->getPhyUploadImgUrl();
?>
<?= $this->Html->css('photoswipe.css'); ?>
<?= $this->Html->css('default-skin/default-skin.css'); ?>

<hr>
<div class="row">
  <div class="col-xs-6 col-lg-3 mt10">
    <?= $this->Form->select('category',$cates,['class'=>'form-control mb20 minw200 maxw300',
                                               'id'=>'change_cate',
                                               'empty'=>["Chọn danh mục"],
                                               'value'=>(!empty($chosen_cate)?$chosen_cate:'0')]); ?>
  </div>
</div>
<div class="row">
  <?php foreach ($products as $product => $value): ?>
  <div class="col-xs-6 col-lg-3 mt10" id="<?= $value->id ?>">
    <div class="products col-lg-12">
      <a href="javascript: void(0)" class="view_detail">
        <div class="reveal text-c">
          <img src="<?= file_exists($path.$value->img_name) == 1 && !empty($value->img_name)?$url.$value->img_name:$baseimgURL."no-image.png" ?>" width="auto" title="" class="pro_img" />
          <input type="hidden" value="<?= $value->id ?>" class="pro_id">
          <input type="hidden" value="<?= $value->price_up ?>" class="price_up">
          <input type="hidden" value="<?= $value->quality ?>" class="quality">
          <div class="hidden">
            <div class="caption">
              <div class="centered">
                <p class="pro_code">#<?= $value->cate_code.$value->pro_code ?></p>
                <p class="rent_price">$20.99</p>
                <p class="sell_price">$19.99</p>
              </div>
            </div>
          </div><!-- hidden -->
          <label class="mt5"><?= $value->cate_code.$value->pro_code ?></label>
        </div>
      </a>
    </div>
  </div><!--/.col-xs-6.col-lg-3-->
  <?php endforeach; ?>
  <input type="hidden" value="<?= ($products?end($products)->id:"") ?>" id="last_pro_id">
</div><!--/row-->

<!-- Popup view images -->
<!-- Root element of PhotoSwipe. Must have class pswp. -->
<div class="pswp" tabindex="-1" role="dialog" aria-hidden="true">
  <!-- Background of PhotoSwipe. 
       It's a separate element as animating opacity is faster than rgba(). -->
  <div class="pswp__bg"></div>
  <!-- Slides wrapper with overflow:hidden. -->
  <div class="pswp__scroll-wrap">
    <!-- Container that holds slides. 
        PhotoSwipe keeps only 3 of them in the DOM to save memory.
        Don't modify these 3 pswp__item elements, data is added later on. -->
    <div class="pswp__container">
        <div class="pswp__item"></div>
        <div class="pswp__item"></div>
        <div class="pswp__item"></div>
    </div>
    <!-- Default (PhotoSwipeUI_Default) interface on top of sliding area. Can be changed. -->
    <div class="pswp__ui pswp__ui--hidden">
      <div class="pswp__top-bar">
        <!--  Controls are self-explanatory. Order can be changed. -->
        <div class="pswp__counter"></div>
        <button class="pswp__button pswp__button--close" title="Close (Esc)"></button>
        <button class="pswp__button pswp__button--share" title="Share"></button>
        <button class="pswp__button pswp__button--fs" title="Toggle fullscreen"></button>
        <button class="pswp__button pswp__button--zoom" title="Zoom in/out"></button>
        <!-- Preloader demo http://codepen.io/dimsemenov/pen/yyBWoR -->
        <!-- element will get class pswp__preloader--active when preloader is running -->
        <div class="pswp__preloader">
            <div class="pswp__preloader__icn">
              <div class="pswp__preloader__cut">
                <div class="pswp__preloader__donut"></div>
              </div>
            </div>
        </div>
      </div>
      <div class="pswp__share-modal pswp__share-modal--hidden pswp__single-tap">
          <div class="pswp__share-tooltip"></div> 
      </div>
      <button class="pswp__button pswp__button--arrow--left" title="Previous (arrow left)">
      </button>
      <button class="pswp__button pswp__button--arrow--right" title="Next (arrow right)">
      </button>
      <div class="pswp__caption">
          <div class="pswp__caption__center"></div>
      </div>
    </div>
  </div>
</div>

<?= $this->Html->script('jquery.number.js'); ?>
<?= $this->Html->script('price.js'); ?>
<?= $this->Html->script('photoswipe.min.js'); ?>
<?= $this->Html->script('photoswipe-ui-default.min.js'); ?>
<script type="text/javascript">
$(document).ready(function()
{
	//Handle the price with origin price
  calPrice();

  //Lazy loading
  $(window).scroll(function() {
		if ($(window).scrollTop() == $(document).height() - $(window).height()) 
		{
      var chosen_cate = "<?= ($chosen_cate?$chosen_cate:null)?>";
      $('#divLoading').addClass('show');
			$.ajax({
        type: "POST",
        url: "<?php echo $this->Url->build( [ 'action' => 'ajax' ] ) ?>",
        data: { load_more: $("#last_pro_id").val(), chosen_cate: chosen_cate },
        success: function(data){
          var Obj = jQuery.parseJSON(data);
          if(Obj != "")
          {
            
            var last_pro_id = $("#last_pro_id").val();
            var html = "";
            //Generate html
            for (var i = 0; i < Obj.data.length; i++)
            {
              var item = Obj.data[i];
              //console.log(item['img_name']);
              var url = "<?= $baseimgURL ?>no-image.png"; //default no image
              var url_img = '<?= $url ?>'+item['img_name'];
              //console.log( url_img );
              if( UrlExists(url_img) == true )
              {
                url = url_img;
              }
              html += "<div class='col-xs-6 col-lg-3 mt10' id='"+item['id']+"'>\
                        <div class='products col-lg-12'>\
                          <a href='javascript: void(0)' class='view_detail'>\
                            <div class='reveal text-c'>\
                              <img src='"+url+"' width='auto' title='' class='pro_img' />\
                              <input type='hidden' value='"+item['id']+"' class='pro_id'>\
                              <input type='hidden' value='"+item['price_up']+"' class='price_up'>\
                              <input type='hidden' value='"+item['quality']+"' class='quality'>\
                              <div class='hidden'>\
                                <div class='caption'>\
                                  <div class='centered'>\
                                    <p class='pro_code'>#"+item['cate_code']+item['pro_code']+"</p>\
                                    <p class='rent_price'>$20.99</p>\
                                    <p class='sell_price'>$19.99</p>\
                                  </div>\
                                </div>\
                              </div>\
                              <label class='mt5'>"+item['cate_code']+item['pro_code']+"</label>\
                            </div>\
                          </a>\
                        </div>\
                      </div>";
              //Set last id for load more function, the last id will be id when loop end
              $("#last_pro_id").val( item['id'] );
            }//end for

            //Append html data to view
            $(html).insertAfter("#"+last_pro_id+"");
            //Call calculate function
            calPrice();
          }
        },
        complete: function(){
          setTimeout(function(){ $('#divLoading').removeClass('show'); }, 500);
        },
        error: function(){
          setTimeout(function(){ $('#divLoading').removeClass('show'); }, 500);
          swal("Đã xảy ra lỗi, hãy thử lại");
        }
      });//end ajax
		}
	});

});//end ready

//View detail product
$(document).on('click',".view_detail",function(){
	var pro_id = $(this).find(".pro_id").val();
  $.ajax({
    type: "POST",
    url: "<?php echo $this->Url->build( [ 'action' => 'ajax' ] ) ?>",
    data: {view_detail: pro_id},
    success: function(data){
      var Obj = jQuery.parseJSON(data);
      if(Obj != "")
      {
        var pswpElement = document.querySelectorAll('.pswp')[0];
        var items = [];
        var url = "<?= $url ?>";
        // build items array
        for (var i = 0; i < Obj.data.length; i++)
        {
          items.push( {
              src: url+Obj.data[i]['img_name'],
              w: 600,
              h: 900
          } );
        }
        
        var options = {
          index: 0 
        };
        // Initializes and opens PhotoSwipe
        var gallery = new PhotoSwipe( pswpElement, PhotoSwipeUI_Default, items, options);
        gallery.init();

      }
    },
    complete: function(){
      $('#divLoading').removeClass('show');
    },
    error: function(){
      $('#divLoading').removeClass('show');
      swal("Đã xảy ra lỗi, hãy thử lại");
    }
  });//end ajax
});

//Calculate price
function calPrice()
{
  $(".price_up").each(function(){
    var ObjPrice = new Price();
    //Set quality and origin price to count rent and sell price
    ObjPrice.setInfo( Number($(this).val()) , Number($(this).next(".quality").val()) );
    //Set calculated data to rent price
    var rent_price = $.number( ObjPrice.calRentPrice(), 0, ',' );
    $(this).next().next('.hidden').find('.rent_price').text(""+rent_price+" VND");
    var sell_price = $.number( ObjPrice.calSellPrice(), 0, ',' );
    $(this).next().next('.hidden').find('.sell_price').text(""+sell_price+" VND");
    //console.log(ObjPrice.calRentPrice());
  });
}

//Action change category change the data of page.
$("#change_cate").on('change',function(){
  var cate_code = $(this).val();
  window.location.replace("/showroom?cate="+cate_code+"");
})

//Check images existed
function UrlExists(url)
{
  var http = new XMLHttpRequest();
  http.open('HEAD', url, false);
  http.send();
  return http.status!=404;
}


</script>
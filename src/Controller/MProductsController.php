<?php
/**
*
* Description: MProducts controller to handle all action in m_clothes table
* Class: MProductsController
* @author: HuyDo
* Path: /src/Controller/MProductsController.php
*
**/
namespace App\Controller;

use App\Controller\AppController;
use Cake\Event\Event;
use Cake\ORM\TableRegistry;
use Cake\Core\Configure;

//Loading the messages in config/message.php
$GLOBALS = ['msg' => Configure::read('msg')];

class MProductsController extends AppController
{
    
    public function initialize()
    {
        parent::initialize();
        $this->loadComponent('Paginator');
    }
    
    public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);
        
        //$this->Auth->allow(['']);
    }
    
    public $components=array('RequestHandler','Paginator');
    
    /**
    ***************************
    * Pages: FUNCTIONS OF PAGES
    ***************************
    **/

    /**
     * Description: The list of products and ADD functions included
     * Function: index
     * @author: HuyDo
     * @params: none
     * @return: object
     * @version: 1.0
     */
    public function index()
    {
        //Set page Title
        $this->set('title', 'Sản phẩm');
        //Initize the variable
        $chosen_cate = "";        

        //Build the query
        $query = $this->MProducts->query();
        $query->select([
                    'id'=>'MProducts.id', 
                    'pro_code'=>'MProducts.pro_code',
                    'cate_code'=>'MProducts.cate_code',
                    'quality'=>'MProducts.quality_stt',
                    'price'=>'MProducts.price',
                    'price_up'=>'MProducts.price_up',
                    'import_date'=>'MProducts.import_date',
                    'is_sold'=>'MProducts.is_sold',
                    ]);

        if( $this->request->is('get') )
        {
            $chosen_cate = $this->request->query("cate");
            $query->where([ 'cate_code' => $chosen_cate]);
        }

        $products = $query;
        
        //Return the taken results
        $cates = $this->__getCategories();
        $suppliers = $this->__getSuppliers();
        $this->set(compact('cates','products','suppliers','chosen_cate'));
    }

        /**
     * Description: Add new products
     * Function: add
     * @author: HuyDo
     * @params: none
     * @return: object
     * @version: 1.0
     */
    public function add()
    {
        if ($this->request->is('post')) {
            
            $this->request->data['price'] = str_replace( ',', '', $this->request->data['price'] );
            $this->request->data['price_up'] = str_replace( ',', '', $this->request->data['price_up'] );
            $this->request->data['import_date'] = preg_replace("/(\d+)\D+(\d+)\D+(\d+)/","$3-$2-$1",$this->request->data['import_date']);
            $this->request->data['created'] = date("Y-m-d h:i:s");
            $this->request->data['modified'] = date("Y-m-d h:i:s");
            //debug( $this->request->data );exit;

            $products = $this->MProducts->newEntity();
            $products = $this->MProducts->patchEntity($products, $this->request->data);
            if ($this->MProducts->save($products)) {
                $this->Flash->success(__( "Sản phẩm đã được thêm vào hệ thống" ));
                return $this->redirect('/products?cate='.$this->request->data['cate_code']);
            }
            $this->Flash->error(__( "Đã có lỗi xảy ra, xin hãy thử lại" ));
        }
    }

    /**
    ***************************
    * AJAX HANDLER
    ***************************
    **/

    /**
     * Description: Ajax action for check some things via ajax method
     * Function: ajax()
     * @author: HuyDo
     * @params: none
     * @return: none
     */
    public function ajax()
    {
        $data = array();

        //Get data to show detail
        if( isset( $this->request->data['view_detail'] ) ) 
        {
            //Get the Product code from request
            $request_data = ($this->request->data['view_detail']); //A0001
            //Split category and product id
            $category = substr( $request_data, 0 , 1 ); //A
            $pro_id = substr( $request_data, 1 , 4 ); //0001
            //Query to get detail data of product
            $data['pro_detail'] = $this->__getdetailProduct( $category , $pro_id );
            //Get detail data's id what queried
            $query_result = $data['pro_detail']->toArray();
            $id = $query_result[0]['id'];
            //Get detail product images by id
            $data['pro_imgs'] = $this->__getdetailImages( $id );
        }
        //Get product category and code to check existance
        elseif( isset( $this->request->data['check_code'] ) ) 
        {
            $data = $this->__checkprocode( $this->request->data['check_code'], $this->request->data['cate_code'] );
        }
        //Update data
        elseif( isset( $this->request->data['update_data'] ) )
        {
            $data = $this->__updateProduct($this->request->data['update_data']);
        }
        //Logic delete record - Mark the product as sold
        elseif ( isset( $this->request->data['sold'] ) ) {
            $data = $this->__soldProduct($this->request->data['sold']);
        }
        //Quick save data
        elseif ( isset( $this->request->data['quick_save'] ) ) {
            $data = $this->__quickSave( $this->request->data['pro_code'] , $this->request->data['edited_data'] , $this->request->data['quick_save']);
        }
        //Find newset product id of the selected category
        elseif ( isset( $this->request->data['find_newest'] ) ) {
            $data = $this->__findNewestId( $this->request->data['find_newest'] );
        }        
        //Quick add category
        elseif ( isset( $this->request->data['qk_add_cate'] ) ) {
            $data = $this->__qkAddCate( $this->request->data['qk_add_cate'] );
        }
        //Check category code is existed
        elseif ( isset( $this->request->data['checkCatecode'] ) ) {
            $data = $this->__checkCatecode( $this->request->data['checkCatecode'] );
        }
        //Check category code is existed
        elseif ( isset( $this->request->data['qk_add_sup'] ) ) {
            $data = $this->__qkAddSup( $this->request->data['qk_add_sup'] );
        }
        
        echo json_encode( [ 'data' => $data ] );
        exit();
    }

    /**
    *
    * PRIVATE FUNCTIONS
    *
    **/

    /**
    * Description: Get the categories data for select box
    * Function: __getCategories()
    * @author: HuyDo
    * @params: none
    * @return: array data
    */
    private function __getCategories()
    {
        $MCategories = TableRegistry::get('MCategories');
        $query = $MCategories->find('list', [
            'keyField' => 'cate_code',
            'valueField' => 'cate_name',
            'conditions'=> ['is_delete' => 0],
        ]);
        return $query->toArray();
    }

    /**
    * Description: Get the suppliers data for select box
    * Function: __getSuppliers()
    * @author: HuyDo
    * @params: none
    * @return: array data
    */
    private function __getSuppliers()
    {
        $MSuppliers = TableRegistry::get('MSuppliers');
        $query = $MSuppliers->find('list', [
            'keyField' => 'id',
            'valueField' => 'sup_name',
            'conditions'=> ['is_delete' => 0],
        ]);
        return $query->toArray();
    }

    /**
    * Description: Get detail data of product
    * Function: __getdetailProduct()
    * @author: HuyDo
    * @params: none
    * @return: array data
    */
    private function __getdetailProduct($category,$pro_id)
    {
        $query = $this->MProducts->query();
        return $query->select([
                    'id'=>'MProducts.id', 
                    'pro_code'=>'MProducts.pro_code',
                    'cate_code'=>'MProducts.cate_code',
                    'color'=>'MProducts.color',
                    'quantity'=>'MProducts.quantity',
                    'quality'=>'MProducts.quality_stt',
                    'price'=>'MProducts.price',
                    'price_up'=>'MProducts.price_up',
                    'import_date'=>'MProducts.import_date',
                    'is_sold'=>'MProducts.is_sold',
                    'sup_id'=>'MProducts.sup_id',
                    'supplier'=>'MSuppliers.sup_name',
                    'supplier_web'=>'MSuppliers.sup_website',
                    'category'=>'MCategories.cate_name',
                    ])
              ->join([
                    'MSuppliers' =>[
                        'table' => 'tsl_suppliers',
                        'type' => 'LEFT',
                        'conditions' => 'MProducts.sup_id = MSuppliers.id'
                    ],
                    'MCategories' =>[
                        'table' => 'tsl_categories',
                        'type' => 'LEFT',
                        'conditions' => 'MProducts.cate_code = MCategories.cate_code'
                    ],
                ])
              ->where(['MProducts.cate_code'=>$category,'MProducts.pro_code'=>$pro_id]);
    }

    /**
    * Description: Get detail images of product
    * Function: __getdetailImages()
    * @author: HuyDo
    * @params: none
    * @return: array data
    */
    private function __getdetailImages($id)
    {
        $query = $this->MProducts->query();
        return $query->select(['img_name'=>'MPhotos.img_name'])
                     ->join([
                            'MPhotos' =>[
                                'table' => 'tsl_photos',
                                'type' => 'LEFT',
                                'conditions' => 'MPhotos.pro_id = MProducts.id'
                            ],
                        ])
                     ->where(['MProducts.id'=>$id]);
    }

    /**
    * Description: Get existed product code
    * Function: __checkprocode()
    * @author: HuyDo
    * @params: $code, $cate
    * @return: array data
    */
    private function __checkprocode($code, $cate)
    {
        $query = $this->MProducts->find()
                       ->where(['pro_code' => $code,
                                'cate_code' => $cate])
                       ->first();
        ;
        if( $query != null ){
            return false;
        }else{
            return true;
        }
    }

    /**
    * Description: Update product data with ajax
    * Function: __updateProduct()
    * @author: HuyDo
    * @params: update_data
    * @return: array data
    */
    private function __updateProduct($update_data)
    {
        $result = false;
        
        if( !empty($update_data['id']) )
        {
            $products = $this->MProducts->newEntity($update_data);
            if( $this->MProducts->save($products) )
            {
                $result = true;
            }
        }
        return $result; 
    }

    /**
    * Description: Update product data with ajax
    * Function: __soldProduct()
    * @author: HuyDo
    * @params: pro_id
    * @return: array data
    */
    private function __soldProduct($pro_id)
    {
        $result = false;
        if( !empty($pro_id) )
        {
            $products = $this->MProducts->newEntity();
            $products->id = $pro_id;
            $products->is_sold = 1;
            $products->modified = date("Y-m-d h:i:s");
            if( $this->MProducts->save($products) )
            {
                $result = true;
            }
        }
        return $result; 
    }

    /**
    * Description: Quick edit product info
    * Function: __quickSave()
    * @author: HuyDo
    * @params: update_data
    * @return: array data
    */
    private function __quickSave($pro_code, $edit_data, $field)
    {
        $result = false;
        
        if( !empty($pro_code) )
        {
            //Split category and product code
            $cate_code= substr( $pro_code, 0 , 1 ); //A
            $pro_code = substr( $pro_code, 1 , 4 ); //0001
            //Find the id of product
            $query = $this->MProducts->find('all',['fields'=>['id']])->where(['cate_code'=>$cate_code,'pro_code'=>$pro_code])->first();
            $id = $query->id;
            //Build the query
            $products = $this->MProducts->newEntity();
            $products->id = $id;
            $products->$field = $edit_data;
            $products->modified = date("Y-m-d h:i:s");
            if( $this->MProducts->save($products) )
            {
                $result = true;
            }
        }
        return $result; 
    }

    /**
    * Description: Quick add category
    * Function: __qkAddCate
    * @author: HuyDo
    * @params: $data
    * @return: array data
    */
    private function __qkAddCate($data)
    {
        $result = false;
        
        if( !empty($data) )
        {
            //Build the query
            $MCategories = TableRegistry::get('MCategories');
            $category = $MCategories->newEntity($data);
            $category->created = date("Y-m-d h:i:s");
            $category->modified = date("Y-m-d h:i:s");
            if( $inserted = $MCategories->save($category) )
            {
                $result = $inserted;
            }
        }
        return $result; 
    }

    /**
    * Description: Find the newest product id when select category
    * Function: __findNewestId()
    * @author: HuyDo
    * @params: $cate_code
    * @return: product newest id
    */
    private function __findNewestId($cate_code)
    {
        if( !empty($cate_code) )
        {
            return $this->MProducts->find('all',['fields'=>['pro_code']])->where(['cate_code'=>$cate_code])->last();
        } 
    }

    /**
    * Description: Check category code is existed or not
    * Function: __checkCatecode()
    * @author: HuyDo
    * @params: $cate_code
    * @return: boolean
    */
    private function __checkCatecode($cate_code)
    {
        $MCategories = TableRegistry::get('MCategories');
        $query = $MCategories->find()
                             ->where(['cate_code' => $cate_code])
                             ->first();
        ;
        if( $query != null ){
            return false; //existed
        }else{ 
            return true; //not existed
        }
    }

    /**
    * Description: Quick add supplier
    * Function: __qkAddSup
    * @author: HuyDo
    * @params: $data
    * @return: array data
    */
    private function __qkAddSup($data)
    {
        $result = false;
        
        if( !empty($data) )
        {
            //Build the query
            $MSuppliers = TableRegistry::get('MSuppliers');
            $supllier = $MSuppliers->newEntity($data);
            $supllier->created = date("Y-m-d h:i:s");
            $supllier->modified = date("Y-m-d h:i:s");
            if( $inserted = $MSuppliers->save($supllier) )
            {
                $result = $inserted;
            }
        }
        return $result; 
    }
  
}//end class

?>
<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link      http://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   http://www.opensource.org/licenses/mit-license.php MIT License
 */
namespace App\Controller;

use Cake\Core\Configure;
use Cake\Network\Exception\NotFoundException;
use Cake\View\Exception\MissingTemplateException;
use Cake\ORM\TableRegistry;
use Cake\Event\Event;

/**
 * Static content controller
 *
 * This controller will render views from Template/Pages/
 *
 * @link http://book.cakephp.org/3.0/en/controllers/pages-controller.html
 */
class PagesController extends AppController
{

  public function initialize()
  {
      parent::initialize();

      //Define layout template for all site
      $this->viewBuilder()->layout("backend");
  }

  public $components=array('RequestHandler','Paginator');

  /**
   * Displays a view
   *
   * @return void|\Cake\Network\Response
   * @throws \Cake\Network\Exception\NotFoundException When the view file could not
   *   be found or \Cake\View\Exception\MissingTemplateException in debug mode.
   */
  public function display()
  {
      $path = func_get_args();

      $count = count($path);
      if (!$count) {
          return $this->redirect('/');
      }
      $page = $subpage = null;

      if (!empty($path[0])) {
          $page = $path[0];
      }
      if (!empty($path[1])) {
          $subpage = $path[1];
      }
      $this->set(compact('page', 'subpage'));

      try {
          $this->render(implode('/', $path));
      } catch (MissingTemplateException $e) {
          if (Configure::read('debug')) {
              throw $e;
          }
          throw new NotFoundException();
      }
  }

  /**
   * Description: Dashboard
   * Function: dashboard
   * @author: HuyDo
   * @params: none
   * @return: none
   */
  public function dashboard()
  {
      //Set page Title
      $this->set('title', 'Dashboard');
  }

  /**
   * Description: View product
   * Function: showroom
   * @author: HuyDo
   * @params: none
   * @return: none
   */
  public function showroom()
  {
      //Set page Title
      $this->set('title', 'Sản phẩm');
      //Set page layout
      $this->viewBuilder()->layout("frontend");
      //Chosen category
      $chosen_cate = "";

      $products = [];
      if( $this->request->is("get") && ( $this->request->query('cate') ) )
      {
        $chosen_cate = $this->request->query("cate");
        $products = $this->__getProductDetail( 8, false, null, $chosen_cate );
      }
      else
      {
        $products = $this->__getProductDetail( 8, false, null, false);
      }
      $cates = $this->__getCategories();
      $this->set(compact("products","cates","chosen_cate"));
  }

  /**
   * Description: Ajax action for check some things via ajax method
   * Function: ajax()
   * @author: HuyDo
   * @params: none
   * @return: none
   */
  public function ajax()
  {
    $data = array();
    //Get data to show detail
    if( isset( $this->request->data['view_detail'] ) ) 
    {
      $data = $this->__getProductImages( $this->request->data['view_detail'] );
    }
    //Load more data
    elseif( isset( $this->request->data['load_more'] ) ) 
    {
      $cate = "";
      if( $this->request->query("cate") != "0" ){ $cate = $this->request->query("cate"); }
      $data = $this->__getProductDetail( 4, true ,$this->request->data['load_more'], $this->request->data["chosen_cate"] );
    }

    echo json_encode( [ 'data' => $data ] );
    exit();
  }

  /**
  *
  * PRIVATE FUNCTIONS
  *
  **/

  /**
  * Description: Get the product data
  * Function: __getProductDetail()
  * @author: HuyDo
  * @params: $num: number data query once
             $load_more: true or false
  * @return: array data
  */
  private function __getProductDetail($num, $load_more = false, $last_id = 0, $cate = false)
  {
    //print_r($last_id);
    //Build the query
    $MProducts = TableRegistry::get('MProducts');
    $query = $MProducts->query();
    $query->select([
                'id'=>'MProducts.id', 
                'pro_code'=>'MProducts.pro_code',
                'cate_code'=>'MProducts.cate_code',
                'quality'=>'MProducts.quality_stt',
                'price_up'=>'MProducts.price_up',
                'img_name'=>'MPhotos.img_name',
                ])
          ->join([
                'MPhotos' => [
                    'table' => 'tsl_photos',
                    'type' => 'LEFT',
                    'conditions' => 'MPhotos.pro_id = MProducts.id'
                ],
              ])
          ->where(['is_sold'=>'0'])
          ->group(['MProducts.id'])
          ->order(['MProducts.id'])
          ->limit($num);
    if( $load_more )
    {
      $query->where(['MProducts.id >'=>$last_id]);
    }
    if( $cate )
    {
      $query->where(['MProducts.cate_code'=>$cate]);
    }
    return $query->toArray();
    // debug( $query->toArray() );
  }

  /**
  * Description: Get the product images
  * Function: __getProductImages()
  * @author: HuyDo
  * @params: $num: pro_id
  * @return: array data
  */
  private function __getProductImages($pro_id)
  {
    //Build the query
    //print_r($pro_id);exit;
    $MPhotos = TableRegistry::get('MPhotos');
    $query = $MPhotos->query();
    return $query->select(['img_name'])
          ->where(['pro_id'=>$pro_id,
                   'is_delete'=>'0'
                  ]);
    // return $query->toArray();
  }

  /**
  * Description: Get the categories data for select box
  * Function: __getCategories()
  * @author: HuyDo
  * @params: none
  * @return: array data
  */
  private function __getCategories()
  {
      $MCategories = TableRegistry::get('MCategories');
      $query = $MCategories->find('list', [
          'keyField' => 'cate_code',
          'valueField' => 'cate_name',
          'conditions'=> ['is_delete' => 0],
      ]);
      return $query->toArray();
  }
}

<?php
/**
*
* Description: Photos controller
* Class: MPhotosController
* @author: HuyDo
* Path: /src/Controller/MPhotosController.php
*
**/
namespace App\Controller;

use App\Controller\AppController;
use Cake\Event\Event;
use Cake\ORM\TableRegistry;
use Cake\Core\Configure;

//Loading the messages in config/message.php
$GLOBALS = ['msg' => Configure::read('msg')];

class MPhotosController extends AppController
{
    
    public function initialize()
    {
      parent::initialize();
      $this->loadComponent('Paginator');
    }
    
    public function beforeFilter(Event $event)
    {
      parent::beforeFilter($event);
      
      //$this->Auth->allow(['']);
    }
    
    public $components=array('RequestHandler','Paginator');

    /**
    ***************************
    * Pages: FUNCTIONS OF PAGES
    ***************************
    **/

    /**
     * Description: The list of photo
     * Function: index()
     * @author: HuyDo
     * @params: none
     * @return: object
     * @version: 1.0
     */
    public function index()
    {
      //Set page Title
      $this->set('title', 'Thư viện hình ảnh');
      //Build the query
      $query = $this->MPhotos->query()->select(['id','img_name'])->where(['is_delete'=>0]);
      $photos = "";
      //Build paginate config
      $this->paginate = [
        'limit' => 20,
        'order' => ['id DESC']
      ];
      $photos = $this->paginate($query);

      //Handler adding the images to products
      if( $this->request->is("post") && $this->request->data["pro_id"] )
      {
        //Build the query
        $img_id = explode("," , $this->request->data['img_id']);
        $count_id = count( $img_id );
        $count_done = 0;
        
        for($i = 0; $i < $count_id; $i++)
        {
            $img_pro = $this->MPhotos->get($img_id[$i]);
            // $data['created'] = date('Y-m-d H:i:s');
            $img_pro['modified'] = date('Y-m-d H:i:s');
            $img_pro['pro_id'] = $this->request->data['pro_id'];
            
            if ( $this->MPhotos->save($img_pro) ) 
            {
                $count_done++;
            }
        }
        
        if ( $count_done == $count_id ) 
        {
            $this->Flash->success(__( "Ảnh đã được thêm vào sản phẩm" ));
            return $this->redirect('/photos');
        }
        else
        {
            $this->Flash->error(__( "Đã có lỗi xảy ra, xin hãy thử lại" ));
        }
      }

      $procodes = $this->__getProcode();
      //Render data to view
      $this->set(compact('photos','procodes'));
    }

    /**
     * Description: Add/upload new photos
     * Function: add()
     * @author: HuyDo
     * @params: none
     * @return: object
     * @version: 1.0
     */
    public function add()
    {
      //Set page Title
      $this->set('title', 'Tải ảnh lên');
      $result = false;
      //Check upload file
      if(!empty($_FILES))
      {
        $targetDir = "upload/pro_images/";
        //Check folder was created
        if (!is_dir( WWW_ROOT.DS."upload".DS."pro_images"))
        {
          mkdir( WWW_ROOT.DS."upload", 0777 );
          mkdir( WWW_ROOT.DS."upload".DS."pro_images", 0777 );
        }
        $fileName = $_FILES['file']['name'];
        $targetFile = $targetDir.$fileName;
        $MPhotos = TableRegistry::get('MPhotos');
        $photos = $MPhotos->newEntity();
        //print_r( $this->response );
        if(move_uploaded_file( $_FILES['file']['tmp_name'], TMP . '/' . $fileName ))
        {
          if($this->__resize_img(TMP, $targetDir, $fileName, 600))
          {
            $data['img_name'] = $fileName;
            $data['created'] = date('Y-m-d H:i:s');
            $data['modified'] = date('Y-m-d H:i:s');
            $photos = $MPhotos->patchEntity($photos, $data);
            
            if ( $MPhotos->save($photos) ) 
            {
              $this->response;
            } 
          } 
          unlink( TMP . '/' . $fileName);
        }
      }
    }

    /**
    ***************************
    * AJAX HANDLER
    ***************************
    **/

    /**
    * Description: Ajax function, solve the request from ajax
    * Function: ajax
    * @author: HuyDo
    * @params: none
    * @return: boolean
    * @version: 1.0
    */
    public function ajax()
    {
        $data = array();
        
        if( isset( $this->request->data['get_pro_imgs'] ) ) 
        {
            $data = $this->__getProimg( $this->request->data['get_pro_imgs'] );
        }
        //Delete the photo from products
        elseif( isset( $this->request->data['del_img_from_pro'] ) ) 
        {
            $data = $this->__delImgFromProduct( $this->request->data['del_img_from_pro'] );
        }
        
        echo json_encode( ['data' => $data] );
        exit();
    }

    /**
    *
    * PRIVATE FUNCTIONS
    *
    **/

    /**
    * Description: Get product code
    * Function: __getProcode()
    * @author: HuyDo
    * @params: $code, $cate
    * @return: array data
    */
    private function __getProcode()
    {
        $MProducts = TableRegistry::get('MProducts');
        return $MProducts->find()
                            ->select(['id','pro_code','cate_code'])
                            ->formatResults(function($results) {
                                return $results->combine(
                                    'id',
                                    function($row) {
                                        return $row['cate_code'] . $row['pro_code'];
                                    }
                                );
                            })
                            ->where(['is_sold' => '0'])
                            ->order(['id DESC'])
                            ->all();
    }

    /**
    * Description: Get product image data
    * Function: __getProimg()
    * @author: HuyDo
    * @params: $id
    * @return: array data
    */
    private function __getProimg($id)
    {
      //Build the query
      return $this->MPhotos->find( 'all' )
          ->join([
              'MProducts' =>[
                  'table' => 'tsl_products',
                  'type' => 'RIGHT',
                  'conditions' => 'MProducts.id = MPhotos.pro_id'
              ],
          ])
          ->select(['id' => 'MProducts.id',
                    'MPhotos.img_name',
                   ])
          ->where( [  'MPhotos.pro_id' => $id,
                      'MPhotos.is_delete' => '0' ] )
          ->all();
    }

    /**
    * Description: Delete photo from products
    * Function: __delImgFromProduct()
    * @author: HuyDo
    * @params: $id
    * @return: array data
    */
    private function __delImgFromProduct($arr_id)
    {
      $result = false;
      $cnt_query_success = 0;
      //Build the query
      $CnnProimgs = TableRegistry::get('CnnProimgs');

      if( !empty($arr_id) )
      {
        //Process each CnnProImg ID
        $arr_id = explode( "," , $arr_id );
        $cnt_query_prepare = count($arr_id);
        for ($i = 0; $i < $cnt_query_prepare ; $i++) { 
          $cpi = $CnnProimgs->get( $arr_id[$i] );
          $cpi->modified = date("Y-m-d h:i:s");
          $cpi->is_delete = 1;
          if( $CnnProimgs->save($cpi) )
          {
            $cnt_query_success++;
          }
        }
        //Check length of array and query number is same.
        if( $cnt_query_prepare == $cnt_query_success )
        {
          $result = true;
        }

      }
      return $result; 
    }

    /**
    * Description: Resize Image
    * Function: __resize_img()
    * @author: HuyDo
    * @params: $dir_in, $dir_out, $imedat='defaultname.jpg', $max=250
    * @return: boolean
    */
    function __resize_img($dir_in, $dir_out, $imedat='defaultname.jpg', $max=250) 
    {

      $img = $dir_in . '/' . $imedat;
      $tmp = explode('.', $imedat);
      $extension = end( $tmp ); //array_pop();
      // print_r($extension);
      switch ($extension){
        case 'jpg':
        case 'jpeg':
          $image = ImageCreateFromJPEG($img);
        break;

        case 'png':
          $image = ImageCreateFromPNG($img);
        break;

        default:
          $image = false;
      }


      if(!$image){
        // not valid img stop processing
        return false; 
      }

      $vis = imagesy($image);
      $sir = imagesx($image);

      if(($vis < $max) && ($sir < $max)) {
        $nvis=$vis; $nsir=$sir;
      } else {
      if($vis > $sir) { $nvis=$max; $nsir=($sir*$max)/$vis;}
        elseif($vis < $sir) { $nvis=($max*$vis)/$sir; $nsir=$max;}
        else { $nvis=$max; $nsir=$max;}
      }

      $out = ImageCreateTrueColor($nsir,$nvis);
      ImageCopyResampled($out, $image, 0, 0, 0, 0, $nsir, $nvis, $sir, $vis);

      switch ($extension){
        case 'jpg':
        case 'jpeg':
          imageinterlace($out ,1);
          ImageJPEG($out, $dir_out . '/' . $imedat, 75);
        break;

        case 'png':
          ImagePNG($out, $dir_out . '/' . $imedat);
        break;

        default:
          $out = false;
      }

      if(!$out){
        return false;
      }

      ImageDestroy($image);
      ImageDestroy($out);

      return true;
    } 

}//end class
?>
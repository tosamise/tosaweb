<?php
/**
*
* Description: Orders controller for handling file
* Class: OrdersController
* @author: HuyDo
* Path: /src/Controller/OrdersController.php
*
**/
namespace App\Controller;

use App\Controller\AppController;
use Cake\Event\Event;
use Cake\ORM\TableRegistry;
use Cake\Core\Configure;


//Loading the messages in config/message.php
$GLOBALS = ['msg' => Configure::read('msg')];

class OrdersController extends AppController
{
    
    public function initialize()
    {
      parent::initialize();
      $this->loadComponent('Paginator');
    }
    
    public function beforeFilter(Event $event)
    {
      parent::beforeFilter($event);
      
      //$this->Auth->allow(['']);
    }
    
    public $components=array('RequestHandler','Paginator');

    /**
    ***************************
    * Pages: FUNCTIONS OF PAGES
    ***************************
    **/

    /**
    * Description: List all order
    * Function: index()
    * @author: HuyDo
    * @params: none
    * @return: boolean
    * @version: 1.0
    */
    public function index()
    {
      //Set page Title
      $this->set('title', 'Danh sách các đơn hàng');
    }

    /**
    * Description: Add new order
    * Function: add()
    * @author: HuyDo
    * @params: none
    * @return: boolean
    * @version: 1.0
    */
    public function add()
    {
      //Set page Title
      $this->set('title', 'Tạo đơn hàng mới');


      $procodes = $this->__getProcode();
      //Render data to view
      $this->set(compact('photos','procodes'));
    }
    

    /**
    ***************************
    * AJAX HANDLER
    ***************************
    **/

    /**
    * Description: Ajax function, solve the request from ajax
    * Function: ajax
    * @author: HuyDo
    * @params: none
    * @return: boolean
    * @version: 1.0
    */
    public function ajax()
    {
        $data = array();
        
        if( 1==1 ) 
        {
          $data = "";
        }
        
        echo json_encode( ['data' => $data] );
        exit();
    }

    /**
    *
    * PRIVATE FUNCTIONS
    *
    **/

    /**
    * Description: Get product code
    * Function: __getProcode()
    * @author: HuyDo
    * @params: $code, $cate
    * @return: array data
    */
    private function __getProcode()
    {
      $MProducts = TableRegistry::get('MProducts');
      return $MProducts->find()
                          ->select(['id','pro_code','cate_code'])
                          ->formatResults(function($results) {
                              return $results->combine(
                                  'id',
                                  function($row) {
                                      return $row['cate_code'] . $row['pro_code'];
                                  }
                              );
                          })
                          ->where(['is_sold' => '0'])
                          ->all();
    }
   

}//end class
?>
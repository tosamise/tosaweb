<?php
/**
*
* Description: Files controller for handling file
* Class: FilesController
* @author: HuyDo
* Path: /src/Controller/FilesController.php
*
**/
namespace App\Controller;

require_once( ROOT . DS . 'vendor' . DS  . 'phpoffice' . DS . 'phpexcel' . DS . 'Classes' . DS . 'PHPExcel.php');
require_once( ROOT . DS . 'vendor' . DS  . 'phpoffice' . DS . 'phpexcel' . DS . 'Classes' . DS . 'PHPExcel' . DS .'IOFactory.php');
require_once( ROOT . DS . 'vendor' . DS  . 'phpoffice' . DS . 'phpexcel' . DS . 'Classes' . DS . 'PHPExcel' . DS .'Settings.php');
require_once( ROOT . DS . 'vendor' . DS  . 'phpoffice' . DS . 'phpexcel' . DS . 'Classes' . DS . 'PHPExcel' . DS . 'Shared' . DS . 'Date.php');


use App\Controller\AppController;
use Cake\Event\Event;
use Cake\ORM\TableRegistry;
use Cake\Core\Configure;

use PHPExcel;
use PHPExcel_IOFactory;
use PHPExcel_Shared_Date;


//Loading the messages in config/message.php
$GLOBALS = ['msg' => Configure::read('msg')];

class FilesController extends AppController
{
    
    public function initialize()
    {
      parent::initialize();
      $this->loadComponent('Paginator');
    }
    
    public function beforeFilter(Event $event)
    {
      parent::beforeFilter($event);
      
      //$this->Auth->allow(['']);
    }
    
    public $components=array('RequestHandler','Paginator');

    /**
    ***************************
    * Pages: FUNCTIONS OF PAGES
    ***************************
    **/

    /**
     * Description: Import excel file
     * Function: import()
     * @author: HuyDo
     * @params: none
     * @return: object
     * @version: 1.0
     */
    public function import()
    {
      //Set page Title
      $this->set('title', 'Nhập file');
      //Upload file before process
      if( !empty($_FILES) )
      {
        //Registry table & build query
        $MProducts = TableRegistry::get('MProducts');

        //Init variable for upload data
        $result = false;
        $targetDir = "upload/pro_images/";
        $fileName = $_FILES['file']['name'];
        $targetFile = $targetDir.$fileName;
        //Do the upload and process the data
        if( move_uploaded_file( $_FILES['file']['tmp_name'], TMP . '/' . $fileName ) )
        {
          $inputFileName = $this->request->data['file']['tmp_name'];
          $filesize = $this->request->data['file']['size'];
          $extension = $this->request->data['file']['type'];

          if( $this->checkFileFormat($inputFileName, $filesize, $extension) )
          {
            //Convert file data to query data
            $data = $this->__readExcel( TMP . '/' . $fileName );
            $count_succeed = 0;
            $count_array = count($data);
            //Process each row data
            foreach ($data as $key)
            {
              //Check data was existed
              if( ! $pro_id = $this->__checkProExist( $key['cate_code'] , $key['pro_code'] ) )
              {
                $products = $MProducts->newEntity($key);
                if ( $MProducts->save($products) ) 
                {
                  $count_succeed++;
                } 
              }
              else
              {
                $products = $MProducts->get($pro_id);
                $products = $MProducts->patchEntity($products, $key);
                // debug($products);exit;
                if ( $MProducts->save($products) ) 
                {
                  $count_succeed++;
                } 
              }
            }
            
            if( $count_array == $count_succeed )
            {
              unlink( TMP . '/' . $fileName);
              $this->Flash->success(__( "Dữ liệu đã thêm vào hệ thống" ));
              return $this->redirect('/products');
            }
          }
          unlink( TMP . '/' . $fileName);
          $this->Flash->error(__( "Định dạng file sai rồi" ));
        }//end move_upload_file
      }
    }

    /**
    ***************************
    * AJAX HANDLER
    ***************************
    **/

    /**
    * Description: Ajax function, solve the request from ajax
    * Function: ajax
    * @author: HuyDo
    * @params: none
    * @return: boolean
    * @version: 1.0
    */
    public function ajax()
    {
        $data = array();
        
        if( 1==1 ) 
        {
          $data = "";
        }
        
        echo json_encode( ['data' => $data] );
        exit();
    }

    /**
    *
    * PRIVATE FUNCTIONS
    *
    **/

    /**
    * Description: check format file valid or not.
    * Function: checkFileFormat($filename, $filesize, $extension)
    * @author: HuyDo
    * @params: $filename, $filesize, $extension
    * @return: true or false.
    */
    public function checkFileFormat($filename, $filesize, $extension)
    {
      $mimetypes = array('application/vnd.openxmlformats-officedocument.spreadsheetml.sheet', 'application/vnd.ms-excel', 'application/vnd.oasis.opendocument.spreadsheet','application/octet-stream','application/vnd.oasis.opendocument.spreadsheet');
      $formatValid = false;
      // debug($extension);exit;
      if (!empty($filename) && $filesize > 0 && in_array($extension,$mimetypes))
      {
        $formatValid = true;
      }
      else
        $formatValid = false;
      return $formatValid;
    }

    /**
    * Description: read Excel file
    * Function: readExcel($filename, $filesize, $extension)
    * @author: HuyDo
    * @params: $inputFileName
    * @return: array data
    */
    private function __readExcel($inputFileName)
    {
      $objPHPExcel = new PHPExcel();
      $inputFileType = PHPExcel_IOFactory::identify($inputFileName);
      $objReader = PHPExcel_IOFactory::createReader($inputFileType);
      $objPHPExcel = $objReader->load($inputFileName);

      //1.1 Get worksheet dimensions
      $sheet = $objPHPExcel->getSheet(0);
      $highestRow = $sheet->getHighestRow();
      $highestColumn = $sheet->getHighestColumn();
      //1.2 Loop through each row of the worksheet in turn
      $data = array();
      for ($row = 2; $row <= $highestRow; $row++)
      {
        $rows = array();
        for ($col = 0; $col <= 9; $col++)
        {
          $rows[$col] = trim($sheet->getCellByColumnAndRow($col, $row)->getValue());
          //Format date time
          if( $col == 0 && !empty($rows[$col]))
          {
            $rows[$col] = date('Y-m-d', PHPExcel_Shared_Date::ExcelToPHP( $rows[$col] ));
          }
        }
        //1.3 Remove empty row.
        if(array_filter($rows))
        {
          array_push($data, $rows);
        }
      }

      $query_data = [];
      foreach ($data as $key => $value) 
      {
        //[0]: pro_code Ex: A0001
        $pro_code = substr( $value[1] , -4);
        $cate_code = substr( $value[1] , 0 , strpos($value[1],$pro_code, 0) );
        $tmp = [
          'import_date' => $value[0], //[0]: imported_date
          'cate_code' => $cate_code,
          'pro_code' => $pro_code,
          'sup_id' => $value[7],
          'quantity' => $value[8],
          'quality_stt' => $value[2],
          'price' => $value[3],
          'price_up' => $value[4],
          'created' => date("Y-m-d h:i:s"),
          'modified' => date("Y-m-d h:i:s"),
          'is_sold' => $value[9],
        ];
        array_push( $query_data , $tmp );
      }
      // debug($query_data);exit;
      return $query_data;
    }
    
    /**
    * Description: Check product info existed
    * Function: checkProExist()
    * @author: HuyDo
    * @params: $filename, $filesize, $extension
    * @return: true or false.
    */
    private function __checkProExist($cate_code, $pro_code)
    {
      $MProducts = TableRegistry::get('MProducts');
      $exists = $MProducts->query()->select(['id'])->where(['cate_code'=>$cate_code,'pro_code'=>$pro_code])->toArray();
      if( $exists )
      {
        $exists = $exists[0]['id'];
      }
      // debug($exists);exit;
      return $exists;
    }

}//end class
?>
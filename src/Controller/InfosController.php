<?php
/**
*
* Description: Information controller
* Class: InfosController
* @author: HuyDo
* Path: /src/Controller/InfosController.php
*
**/
namespace App\Controller;

use App\Controller\AppController;
use Cake\Event\Event;
use Cake\ORM\TableRegistry;
use Cake\Core\Configure;

//Loading the messages in config/message.php
$GLOBALS = ['msg' => Configure::read('msg')];

class InfosController extends AppController
{
    
    public function initialize()
    {
      parent::initialize();
      $this->loadComponent('Paginator');
    }
    
    public function beforeFilter(Event $event)
    {
      parent::beforeFilter($event);
      
      //$this->Auth->allow(['']);
    }
    
    public $components=array('RequestHandler','Paginator');

    /**
    ***************************
    * Pages: FUNCTIONS OF PAGES
    ***************************
    **/

    /**
     * Description: The list of category and ADD functions included
     * Function: category
     * @author: HuyDo
     * @params: none
     * @return: object
     * @version: 1.0
     */
    public function category()
    {
      //Set page Title
      $this->set('title', 'Danh mục');

      //Build the query
      $MCategories = TableRegistry::get('MCategories');
      $query = $MCategories->query();
      $query->select([
                  'id'=>'MCategories.id', 
                  'cate_code'=>'MCategories.cate_code',
                  'cate_name'=>'MCategories.cate_name',
                  'created'=>'MCategories.created',
                  'modified'=>'MCategories.modified',
                  ])
            ->where(['is_delete'=>0]);
          
      $categories = $query->toArray();
      //Render data to view
      $this->set(compact('categories'));
    }
    
    /**
     * Description: The list of supplier and ADD functions included
     * Function: index
     * @author: HuyDo
     * @params: none
     * @return: object
     * @version: 1.0
     */
    public function supplier()
    {
      //Set page Title
      $this->set('title', 'Nhà cung cấp');

      //Build the query
      $MSuppliers = TableRegistry::get('MSuppliers');
      $query = $MSuppliers->query()->where(['is_delete'=>0]);
          
      $suppliers = $query->toArray();
      //Render data to view
      $this->set(compact('suppliers'));
    }

    /**
    ***************************
    * AJAX HANDLER
    ***************************
    **/

    /**
     * Description: Ajax action for check some things via ajax method
     * Function: ajax()
     * @author: HuyDo
     * @params: none
     * @return: none
     */
    public function ajax()
    {
      $data = array();

      //Quick update category data
      if ( isset( $this->request->data['quick_update_cate'] ) ) 
      {
        $cate_id = $this->request->data['quick_update_cate'];
        $edit_data = $this->request->data['edited_data'];
        $old_data = $this->request->data['old_data'];
        $data = $this->__quickSaveCate($cate_id, $edit_data, $old_data);
      }
      //Quick update supplier data
      elseif( isset( $this->request->data['quick_update_sup'] ) ) 
      {
        $cate_id = $this->request->data['quick_update_sup'];
        $edit_data = $this->request->data['edited_data'];
        $old_data = $this->request->data['old_data'];
        $data = $this->__quickSaveSup($cate_id, $edit_data, $old_data);
      }
      //View supplier detail
      elseif( isset( $this->request->data['view_sup_detail'] ) ) 
      {
        $data = $this->__viewSupDetail( $this->request->data['view_sup_detail'] );
      }
      //Update data supplier
      elseif( isset( $this->request->data['update_data'] ) ) 
      {
        $data = $this->__UpdateSupInfo( $this->request->data['update_data'] );
      }
      //Add supplier
      elseif( isset( $this->request->data['add_supplier'] ) ) 
      {
        $data = $this->__AddSupInfo( $this->request->data['add_supplier'] );
      }
      
      echo json_encode( [ 'data' => $data ] );
      exit();
    }

    /**
    *
    * PRIVATE FUNCTIONS
    *
    **/

    /**
    * Description: Quick edit category info
    * Function: __quickSaveCate()
    * @author: HuyDo
    * @params: update_data
    * @return: array data
    */
    private function __quickSaveCate($cate_id, $edit_data, $old_data)
    {
      $result = false;
      
      if( !empty($cate_id) )
      {
        //Check category info before save
        $MCategories = TableRegistry::get('MCategories');
        $entity = $MCategories->find('all')
                             ->where(['id'=>$cate_id,
                                      'cate_name'=>$old_data])
                             ->first();
        $id = $entity->id;
        if( $id == $cate_id )
        {
          //Build the query
          $entity->id = $cate_id;
          $entity->cate_name = $edit_data;
          $entity->modified = date("Y-m-d h:i:s");
          if( $MCategories->save($entity) )
          {
            $result = true;
          }
        }
      }
      return $result; 
    }

    /**
    * Description: Quick edit supplier info
    * Function: __quickSaveSup()
    * @author: HuyDo
    * @params: update_data
    * @return: array data
    */
    private function __quickSaveSup($sup_id, $edit_data, $old_data)
    {
      $result = false;
      
      if( !empty($sup_id) )
      {
        //Check category info before save
        $MSuppliers = TableRegistry::get('MSuppliers');
        $entity = $MSuppliers->find('all')
                             ->where(['id'=>$sup_id,
                                      'sup_phone'=>$old_data])
                             ->first();
        $id = $entity->id;
        if( $id == $sup_id )
        {
          //Build the query
          $entity->id = $sup_id;
          $entity->sup_phone = $edit_data;
          $entity->modified = date("Y-m-d h:i:s");
          if( $MSuppliers->save($entity) )
          {
            $result = true;
          }
        }
      }
      return $result; 
    }

    /**
    * Description: Update supplier info
    * Function: __UpdateSupInfo()
    * @author: HuyDo
    * @params: update_data
    * @return: array data
    */
    private function __UpdateSupInfo($data)
    {
      $result = false;
      
      if( !empty($data) )
      {
        //Check category info before save
        $MSuppliers = TableRegistry::get('MSuppliers');
        $suppliers = $MSuppliers->newEntity($data);
        //Build the query
        $suppliers->modified = date("Y-m-d h:i:s");
        if( $MSuppliers->save($suppliers) )
        {
          $result = true;
        }
      }
      return $result; 
    }

    /**
    * Description: Add supplier info
    * Function: __AddSupInfo()
    * @author: HuyDo
    * @params: update_data
    * @return: array data
    */
    private function __AddSupInfo($data)
    {
      $result = false;
      
      if( !empty($data) )
      {
        //Check category info before save
        $MSuppliers = TableRegistry::get('MSuppliers');
        $suppliers = $MSuppliers->newEntity($data);
        //Build the query
        $suppliers->created = date("Y-m-d h:i:s");
        $suppliers->modified = date("Y-m-d h:i:s");
        if( $MSuppliers->save($suppliers) )
        {
          $result = true;
        }
      }
      return $result; 
    }

    /**
    * Description: View supplier detail
    * Function: __viewSupDetail()
    * @author: HuyDo
    * @params: update_data
    * @return: array data
    */
    private function __viewSupDetail($sup_id)
    {
      //Check category info before save
      $MSuppliers = TableRegistry::get('MSuppliers');
      return $MSuppliers->query()->where(['id'=>$sup_id,'is_delete'=>0]);
    }


}//end class

?>
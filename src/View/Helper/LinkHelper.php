<?php
/* src/View/Helper/LinkHelper.php */
namespace App\View\Helper;

use Cake\View\Helper;

class LinkHelper extends Helper
{
  public function getBaseImgUrl()
  {
  	if( strpos($_SERVER['HTTP_HOST'], "tosalamour") !== false )
  	{
  		return "http://".$_SERVER['HTTP_HOST'].'/img/';
  	}
  	return "http://".$_SERVER['HTTP_HOST'].'/'.basename(dirname(APP)).'/img/';
  }

  public function getUploadImgUrl()
  {
  	return $this->getBaseImgUrl()."../upload/pro_images/";
  }

  public function getPhyUploadImgUrl()
  {
  	return IMG_UPLOAD . "pro_images" . DS;
  }

}
?>
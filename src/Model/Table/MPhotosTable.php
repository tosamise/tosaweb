<?php  
// src/Model/Table/MPhotosTable.php
namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\ORM\RulesChecker;
use Cake\ORM\Rule\IsUnique;
use Cake\Core\Configure;

//Loading the messages in config/message.php
$GLOBALS = ['msg' => Configure::read('msg')];

class MPhotosTable extends Table
{
    public function initialize(array $config)
    {
        //parent::initialize();
        //Define used table name in database
        $this->table('tsl_photos');
        
    }//end initialize
    
}//end class
?> 
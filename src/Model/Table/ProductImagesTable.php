<?php  
// src/Model/Table/ProductImagesTable.php.php
namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\ORM\RulesChecker;
use Cake\ORM\Rule\IsUnique;
use Cake\Core\Configure;

//Loading the messages in config/message.php
$GLOBALS = ['msg' => Configure::read('msg')];

class ProductImagesTable.php extends Table
{
    public function initialize(array $config)
    {
        //parent::initialize();
        //Define used table name in database
        $this->table('tsl_cnnproimgs');
        
    }//end initialize
    
}//end class
?> 